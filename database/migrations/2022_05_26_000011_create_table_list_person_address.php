<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableListPersonAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.list_person_address', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('address_id');
            $table->uuid('person_id');
            $table->boolean('main')->default(false);
            $table->boolean('active')->default(false);
            $table->foreign('address_id')->references('id')->on('user.address');
            $table->foreign('person_id')->references('id')->on('user.person');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.list_person_address');
    }
}
