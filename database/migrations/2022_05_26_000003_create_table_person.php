<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.person', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('name');
            $table->date('birth_date')->nullable();
            $table->text('cnh', 11)->nullable();
            $table->text('cpf', 14);
            $table->text('email');
            $table->text('phone')->nullable();
            $table->uuid('photo_id')->nullable();
            $table->uuid('sex_id')->nullable();
            $table->foreign('photo_id')->references('id')->on('user.photo');
            $table->foreign('sex_id')->references('id')->on('user.sex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.person');
    }
}
