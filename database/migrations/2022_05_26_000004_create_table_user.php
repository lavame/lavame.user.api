<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.user', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('person_id')->nullable();
            $table->text('login');
            $table->text('password');
            $table->boolean('status')->default(true);
            $table->timestamp('status_date_at')->nullable();
            $table->foreign('person_id')->references('id')->on('user.person');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.user');
    }
}
