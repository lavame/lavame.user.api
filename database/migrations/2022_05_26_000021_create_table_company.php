<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company.company', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('name')->nullable();
            $table->text('cnpj')->nullable();
            $table->text('email')->nullable();
            $table->text('latitude')->nullable();
            $table->text('longitude')->nullable();
            $table->timestamp('closing_time');
            $table->timestamp('opening_time');
            $table->text('phone');
            $table->double('commission')->nullable();
            $table->uuid('bank_id')->nullable();
            $table->uuid('address_id')->nullable();;
            $table->foreign('bank_id')->references('id')->on('company.bank');
            $table->foreign('address_id')->references('id')->on('user.address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.company');
    }
}
