<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableListCompanyCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company.list_company_coupon', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('company_id');
            $table->uuid('coupon_id');
            $table->boolean('disabled')->default(false);
            $table->uuid('disabled_by');
            $table->foreign('disabled_by')->references('id')->on('user.user');
            $table->foreign('company_id')->references('id')->on('company.company');
            $table->foreign('coupon_id')->references('id')->on('user.coupon');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.list_company_coupon');
    }
}
