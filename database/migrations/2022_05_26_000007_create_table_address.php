<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.address', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('zip_code');
            $table->text('street');
            $table->text('number');
            $table->text('complement')->nullable();
            $table->uuid('city_id');
            $table->uuid('state_id');
            $table->foreign('city_id')->references('id')->on('user.city');
            $table->foreign('state_id')->references('id')->on('user.state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.address');
    }
}
