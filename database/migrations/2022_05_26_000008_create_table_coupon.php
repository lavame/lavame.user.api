<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCoupon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.coupon', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('name');
            $table->text('code');
            $table->text('description')->nullable();
            $table->integer('amount');
            $table->integer('minimum_value');
            $table->integer('discount_value');
            $table->timestamp('expiration_date');
            $table->uuid('create_coupon_user_id');
            $table->boolean('status')->default(true);
            $table->timestamp('created_at')->nullable();
            $table->foreign('create_coupon_user_id')->references('id')->on('user.user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.coupon');
    }
}
