<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableListUserCoupons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.list_user_coupon', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('coupon_id');
            $table->boolean('disabled')->default(false);
            $table->uuid('disabled_by')->nullable();
            $table->boolean('used')->default(false);
            $table->timestamp('used_date')->nullable();
            $table->foreign('disabled_by')->references('id')->on('user.user');
            $table->foreign('user_id')->references('id')->on('user.user');
            $table->foreign('coupon_id')->references('id')->on('user.coupon');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.list_user_coupon');
    }
}
