<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //refazer
        Schema::create('company.order', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->date('date');
            $table->date('start_date')->nullable();
            $table->date('final_date')->nullable();
            $table->date('scheduling')->nullable();
            $table->text('coupon_id')->nullable();
            $table->uuid('service_modality_id');
            $table->uuid('status_id');
            $table->uuid('user_id');
            $table->uuid('company_id');
            $table->uuid('vehicle_id');
            $table->uuid('payment_id');
            $table->foreign('service_modality_id')->references('id')->on('company.list_service_modality');
            $table->foreign('status_id')->references('id')->on('company.order_status');
            $table->foreign('user_id')->references('id')->on('user.user');
            $table->foreign('company_id')->references('id')->on('company.company');
            $table->foreign('vehicle_id')->references('id')->on('user.vehicle');
            $table->foreign('payment_id')->references('id')->on('company.payment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.list_company_service');
    }
}
