<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableListCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company.list_company_user', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('company_id');
            $table->uuid('user_id');
            $table->timestamp('date_active')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('status')->default(true);
            $table->foreign('company_id')->references('id')->on('company.company');
            $table->foreign('user_id')->references('id')->on('user.user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.list_company_user');
    }
}
