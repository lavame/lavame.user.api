<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableListServiceModality extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //refazer
        Schema::create('company.list_service_modality', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->float('value');
            $table->float('value_dirt');
            $table->uuid('company_id');
            $table->uuid('service_id');
            $table->foreign('company_id')->references('id')->on('company.company');
            $table->foreign('service_id')->references('id')->on('company.service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.list_service_modality');
    }
}
