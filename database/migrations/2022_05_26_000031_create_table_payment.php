<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //refazer
        Schema::create('company.payment', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->float('value_total');
            $table->float('value_company');
            $table->float('value_system');
            $table->boolean('received_by_company');
            $table->uuid('status_id');
            $table->uuid('type_id');
            $table->foreign('status_id')->references('id')->on('company.payment_status');
            $table->foreign('type_id')->references('id')->on('company.payment_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company.payment');
    }
}
