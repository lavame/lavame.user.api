<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVehicle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user.vehicle', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('license_plate')->nullable();
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->uuid('model_id');
            $table->uuid('year_id');
            $table->uuid('brand_id');
            $table->uuid('color_id');
            $table->uuid('modality_id');
            $table->foreign('model_id')->references('id')->on('user.vehicle_model');
            $table->foreign('year_id')->references('id')->on('user.vehicle_year');
            $table->foreign('brand_id')->references('id')->on('user.vehicle_brand');
            $table->foreign('color_id')->references('id')->on('user.vehicle_color');
            $table->foreign('modality_id')->references('id')->on('user.vehicle_modality');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user.vehicle');
    }
}
