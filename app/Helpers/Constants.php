<?php

namespace App\Helpers;

class Constants
{
    public const GROUP_ADMIN   = 'ffeb87bf-b38d-4b87-a6f3-d2ed672e6685';
    public const GROUP_SUPPORT = '89cb05dd-0255-4022-bc14-1aa5d78ec485';

    public const PAYMENT_TYPE_PIX = '667e3507-0475-4f3e-a923-421cf68ea6c6';
    public const PAYMENT_TYPE_CARD = '4d12d82c-cab6-4903-9f39-7a038cf38592';
    public const PAYMENT_TYPE_CASH = '01fe3e1a-2138-4e1e-a4e2-0b0a55e3860c';

    public const PAYMENT_STATUS_APPROVED = '7c581b7f-ad19-4128-8cce-01c524a4bc07';
    public const PAYMENT_STATUS_WAITING = '366bb282-3eaa-4104-9d1c-9fb4f47a352f';
    public const PAYMENT_STATUS_CANCELED = 'e55bfd1f-57f0-434c-bd61-eb1f083e334c';

    public const ORDER_STATUS_DONE = 'ceb1a342-227a-4588-9b79-a3013115d020';
    public const ORDER_STATUS_ACCEPTED = 'ac16177c-965c-4059-8c96-ea112610be89';
    public const ORDER_STATUS_SCHEDULED = '5d7f6a93-c731-48ef-9e9b-2a14887311c8';
    public const ORDER_STATUS_REFUSED = '98f0dd5b-5126-44dc-b921-0a95ddb7bc1e';
    public const ORDER_STATUS_PROGRESS = 'c1e123fe-d050-4aad-acd3-05717022590f';
    public const ORDER_STATUS_FINISHED = '60317343-3c13-4431-8cac-12656ab66107';
    public const ORDER_STATUS_CANCELED = '6bb38fbf-7a52-440e-98c5-c54064bfbe48';
    public const ORDER_STATUS_CLOSED = 'b92242d4-b4e7-4988-93e9-343eee5b5d08';
}
