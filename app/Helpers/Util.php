<?php

namespace App\Helpers;

use App\Models\Person\Person;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Util
{
    public static function formatDatePatternUSA(string $date)
    {
        return !$date ? null : Carbon::createFromFormat('Y-m-d', $date)->format('Y-m-d');
    }

    public static function formatDateTimePatternUSA(string $date)
    {
        return !$date ? null : Carbon::createFromFormat('d/m/Y H:i:s', $date)->format('Y-m-d H:i:s');
    }

    public static function formatDatePatternBR(string $date)
    {
        return !$date ? null : Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public static function formatDateTimePatternBR($date)
    {
        return !$date ? null : Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i:s');
    }

    public static function sendEmailUser(array $options)
    {
        if (!Arr::get($options, 'user_name')) {
            $person = Person::query()->where('id', '=', auth()->user()->person_id)->first();
            $options['user_name'] =  $person->name;
            $options['user_email'] = $person->email;
        }

        Mail::send(
            'emails.register',
            [
                'user' => Arr::get($options, 'user_name'),
                'email' => Arr::get($options, 'user_email'),
                'text' => Arr::get($options, 'text'),
                'subject' => Arr::get($options, 'subject'),
                'date' => Carbon::now()->format('d/m/Y H:m:s')
            ],
            function ($m) use ($options) {
                $m->from(env('MAIL_FROM'), 'LAVAME');
                $m->to(Arr::get($options, 'user_email'))
                    ->subject('LAVAME - ' . Arr::get($options, 'subject'));
                if (Arr::get($options, 'bcc')) {
                    $m->bcc(Arr::get($options, 'bcc'));
                }
            }
        );
    }
}
