<?php

namespace App\Models\Person;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user.person';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;


    protected $fillable = [
        'id',
        'name',
        'birth_date',
        'cnh',
        'cpf',
        'email',
        'phone',
        'photo_id',
        'sex_id',
    ];
}
