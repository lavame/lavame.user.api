<?php

namespace App\Models\Address;

use Illuminate\Database\Eloquent\Model;

class ListAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user.list_person_address';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'id',
        'address_id',
        'person_id',
        'main',
    ];

    //Relations
    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id')
            ->with(['city', 'state']);
    }
}
