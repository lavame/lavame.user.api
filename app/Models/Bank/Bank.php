<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company.bank';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;


    protected $fillable = [
        'id',
        'name',
        'agency',
        'account',
    ];
}
