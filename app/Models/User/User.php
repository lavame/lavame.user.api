<?php

namespace App\Models\User;

use App\Models\Coupon\CouponUser;
use App\Models\Group\GroupUser;
use App\Models\Person\Person;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        //voltar os grupos
        return [
            'id' => $this->id
        ];
    }

    protected $table      = 'user.user';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'person_id',
        'login',
        'password',
        'created_at',
        'status',
        'status_date_at'
    ];

    //Relations
    public function person()
    {
        return $this->hasOne(Person::class, 'id', 'person_id');
    }

    public function groups()
    {
        return $this->hasMany(GroupUser::class, 'user_id', 'id');
    }

    public function coupons()
    {
        return $this->hasMany(CouponUser::class, 'user_id', 'id')
            ->join('user.coupon', 'user.list_user_coupon.coupon_id', '=', 'user.coupon.id');
    }

    //Scope
    public function scopeOfUser($query)
    {
    }
}
