<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table      = 'company.payment';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'value_total',
        'value_company',
        'value_system',
        'status_id',
        'type_id',
        'received_by_company',
        'value_coupon',
        'check_value_dirt'
    ];

    //Relations
    public function paymentStatus()
    {
        return $this->hasOne(PaymentStatus::class, 'id', 'status_id');
    }

    public function paymentType()
    {
        return $this->hasOne(PaymentType::class, 'id', 'type_id');
    }
}
