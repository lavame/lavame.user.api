<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    protected $table      = 'company.payment_type';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'name'
    ];
}
