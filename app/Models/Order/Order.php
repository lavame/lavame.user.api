<?php

namespace App\Models\Order;

use App\Models\Company\Company;
use App\Models\Company\ServiceModality;
use App\Models\Order\OrderStatus;
use App\Models\Payment\Payment;
use App\Models\User\User;
use App\Models\Vehicle\Vehicle;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table      = 'company.order';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'date',
        'accepted_date',
        'start_date',
        'final_date',
        'scheduling',
        'coupon_id',
        'service_modality_id',
        'status_id',
        'user_id',
        'company_id',
        'vehicle_id',
        'payment_id',
    ];

    //Relations
    public function serviceModality()
    {
        return $this->hasOne(ServiceModality::class, 'id', 'service_modality_id')
            ->with(['service']);
    }

    public function orderStatus()
    {
        return $this->hasOne(OrderStatus::class, 'id', 'status_id')
            ->select('id', 'name');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id')
            ->select('id', 'name', 'cnpj', 'phone');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id', 'vehicle_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'id', 'payment_id')
            ->with(['paymentStatus', 'paymentType']);
    }
}
