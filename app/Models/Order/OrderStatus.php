<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table      = 'company.order_status';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'name',
    ];
}
