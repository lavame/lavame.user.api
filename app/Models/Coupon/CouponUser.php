<?php

namespace App\Models\Coupon;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class CouponUser extends Model
{
    protected $table      = 'user.list_user_coupon';

    protected $primaryKey = 'id';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $keyType    = 'Uuid';

    protected $fillable = [
        'id',
        'user_id',
        'coupon_id',
        'disabled',
        'disabled_by',
        'created_at',
        'used',
        "used_date",
    ];

    //Relations
    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id')
            ->selectRaw("TO_CHAR(expiration_date, 'DD/MM/YYYY HH24:MI:SS') as expiration_date, name, id,description,amount,minimum_value,discount_value,status, code");
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')
            ->select('id', 'status', 'person_id', 'created_at', 'status', 'status_date_at');
    }
}
