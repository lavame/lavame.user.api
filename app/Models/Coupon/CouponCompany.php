<?php

namespace App\Models\Coupon;

use App\Models\Company\Company;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class CouponCompany extends Model
{
    protected $table      = 'company.list_company_coupon';

    protected $primaryKey = 'id';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $keyType    = 'Uuid';

    protected $fillable = [
        'id',
        'company_id',
        'coupon_id',
        'disabled',
        'disabled_by',
        'created_at',
    ];

    //Relations
    public function coupon()
    {
        return $this->hasOne(Coupon::class, 'id', 'coupon_id')
            ->selectRaw("TO_CHAR(expiration_date, 'DD/MM/YYYY HH24:MI:SS') as expiration_date, name, id,description,amount,minimum_value,discount_value,status");
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
