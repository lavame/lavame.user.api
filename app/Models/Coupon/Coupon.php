<?php

namespace App\Models\Coupon;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table      = 'user.coupon';

    protected $primaryKey = 'id';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $keyType    = 'Uuid';

    protected $fillable = [
        'id',
        'name',
        'code',
        'description',
        'amount',
        'minimum_vale',
        'expiration_date',
        'status',
        'status_date_at',
    ];

    public function coupon()
    {
        return $this->hasMany(CouponUser::class, 'id', 'coupon_id');
    }
}
