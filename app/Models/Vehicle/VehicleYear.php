<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;

class VehicleYear extends Model
{
    protected $table      = 'user.vehicle_year';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'name',
    ];
}
