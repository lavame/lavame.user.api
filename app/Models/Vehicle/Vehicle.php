<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table      = 'user.vehicle';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'license_plate',
        'name',
        'description',
        'model_id',
        'year_id',
        'brand_id',
        'color_id',
        'updated_at',
    ];

    //Relations
    public function model()
    {
        return $this->hasOne(VehicleModel::class, 'id', 'model_id');
    }

    public function year()
    {
        return $this->hasOne(VehicleYear::class, 'id', 'year_id');
    }

    public function brand()
    {
        return $this->hasOne(VehicleBrand::class, 'id', 'brand_id');
    }

    public function color()
    {
        return $this->hasOne(VehicleColor::class, 'id', 'color_id');
    }

    public function modality()
    {
        return $this->hasOne(VehicleModality::class, 'id', 'modality_id');
    }
}
