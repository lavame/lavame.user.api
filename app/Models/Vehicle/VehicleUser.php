<?php

namespace App\Models\Vehicle;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class VehicleUser extends Model
{
    protected $table      = 'user.list_vehicle';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'user_id',
        'vehicle_id',
        'active',
        'main',
    ];

    //Relations
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')
            ->select('id');
    }

    public function vehicle()
    {
        return $this->hasOne(Vehicle::class, 'id', 'vehicle_id')
            ->with(['model', 'year', 'brand', 'color', 'modality']);
    }
}
