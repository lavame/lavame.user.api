<?php

namespace App\Models\Company;

use App\Models\Vehicle\VehicleModality;
use Illuminate\Database\Eloquent\Model;

class CompanyService extends Model
{
    protected $table      = 'company.list_company_service';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'company_id',
        'service_id',
        'value',
        'modality_id',
        'active'
    ];

    //Relations
    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function modality()
    {
        return $this->hasOne(VehicleModality::class, 'id', 'modality_id');
    }
}
