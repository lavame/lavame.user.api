<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table      = 'company.service';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'name',
        'value',
        'description',
    ];
}
