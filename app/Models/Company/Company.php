<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'company.company';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'name',
        'cnpj',
        'email',
        'closing_time',
        'opening_time',
        'latitude',
        'longitude',
        'phone',
        'commission',
        'back_id',
        'address_id',
    ];
}
