<?php

namespace App\Models\Company;

use App\Models\User\User;
use App\Models\Vehicle\VehicleModality;
use Illuminate\Database\Eloquent\Model;

class ServiceModality extends Model
{
    protected $table      = 'company.list_service_modality';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'value',
        'service_id',
        'modality_id',
    ];

    //Relations
    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function modality()
    {
        return $this->hasOne(VehicleModality::class, 'id', 'modality_id');
    }
}
