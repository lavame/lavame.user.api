<?php

namespace App\Models\Company;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
    protected $table      = 'company.list_company_user';

    protected $primaryKey = 'id';

    protected $keyType    = 'Uuid';

    public $incrementing  = false;

    public $timestamps    = false;

    protected $fillable = [
        'id',
        'user_id',
        'company_id',
        'active',
        'status',
        'data_active'
    ];

    //Relations
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')
            ->select('id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
