<?php

namespace App\Http\Requests\Vehicle;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_model_id'    => 'required|string',
            'vehicle_year_id'     => 'required|string',
            'vehicle_brand_id'    => 'required|string',
            'vehicle_color_id'    => 'required|string',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Lang::get('common.required'),
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'vehicle_model_id'    =>  Lang::get('vehicle.request.vehicle_model_id'),
            'vehicle_year_id'     =>  Lang::get('vehicle.request.vehicle_year_id'),
            'vehicle_brand_id'    =>  Lang::get('vehicle.request.vehicle_brand_id'),
            'vehicle_color_id'    =>  Lang::get('vehicle.request.vehicle_color_id'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = '';

        foreach ($validator->errors()->messages() as $value) {
            $message .= $value[0] . '<br>';
        }

        throw new HttpResponseException(response()->json([
            'message' => $message,
            'status' => false
        ], 422));
    }
}
