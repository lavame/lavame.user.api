<?php

namespace App\Http\Requests\Address;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_zip_code'   => 'required|string',
            'address_street'     => 'required|string',
            'address_city_id'    => 'required|string',
            'address_state_id'   => 'required|string',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Lang::get('common.required'),
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'address_zip_code'   =>  Lang::get('address.request.address_zip_code'),
            'address_street'     =>  Lang::get('address.request.address_street'),
            'address_city_id'    =>  Lang::get('address.request.address_city_id'),
            'address_state_id'   =>  Lang::get('address.request.address_state_id'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = '';

        foreach ($validator->errors()->messages() as $value) {
            $message .= $value[0] . '<br>';
        }

        throw new HttpResponseException(response()->json([
            'message' => $message,
            'status' => false
        ], 422));
    }
}
