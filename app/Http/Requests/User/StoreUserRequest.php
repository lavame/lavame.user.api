<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_login'    => 'required|string',
            'user_password' => 'required|string',
            'user_cpf'      => 'required|string',
            'user_email'    => 'required|string',
            'user_name'     => 'required|string',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Lang::get('common.required'),
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_login'    => Lang::get('user.request.user_login'),
            'user_password' => Lang::get('user.request.user_password'),
            'user_cpf'      => Lang::get('user.request.user_cpf'),
            'user_email'    => Lang::get('user.request.user_email'),
            'user_name'     => Lang::get('user.request.user_name'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = '';

        foreach ($validator->errors()->messages() as $value) {
            $message .= $value[0] . '<br>';
        }

        throw new HttpResponseException(response()->json([
            'message' => $message,
            'success' => false
        ], 422));
    }
}
