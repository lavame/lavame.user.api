<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string',
            'company_cnpj' => 'required|string',
            'company_email' => 'required|string',
            'company_closing_time' => 'required|string',
            'company_opening_time' => 'required|string',
            'company_phone' => 'required|string',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Lang::get('common.required'),
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'company_name' => Lang::get('company.request.company_name'),
            'company_cnpj' => Lang::get('company.request.company_cnpj'),
            'company_email' => Lang::get('company.request.company_email'),
            'company_closing_time' => Lang::get('company.request.company_closing_time'),
            'company_opening_time' => Lang::get('company.request.company_opening_time'),
            'company_phone' => Lang::get('company.request.company_phone'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = '';

        foreach ($validator->errors()->messages() as $value) {
            $message .= $value[0] . '<br>';
        }

        throw new HttpResponseException(response()->json([
            'message' => $message,
            'status' => false
        ], 422));
    }
}
