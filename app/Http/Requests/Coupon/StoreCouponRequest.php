<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coupon_name'            => 'required|string',
            'coupon_description'     => 'required|string',
            'coupon_amount'          => 'required|integer',
            'coupon_minimum_value'   => 'required|integer',
            'coupon_expiration_date' => 'required|date_format:d/m/Y H:i:s',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Lang::get('common.required'),
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'coupon_name'            => Lang::get('coupon.request.coupon_name'),
            'coupon_description'     => Lang::get('coupon.request.coupon_description'),
            'coupon_amount'          => Lang::get('coupon.request.coupon_amount'),
            'coupon_minimum_value'   => Lang::get('coupon.request.coupon_minimum_value'),
            'coupon_expiration_date' => Lang::get('coupon.request.coupon_expiration_date'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = '';

        foreach ($validator->errors()->messages() as $value) {
            $message .= $value[0] . '<br>';
        }

        throw new HttpResponseException(response()->json([
            'message' => $message,
            'status' => false
        ], 422));
    }
}
