<?php

namespace App\Http\Controllers\User\Vehicle;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vehicle\StoreVehicleRequest;
use App\Repositories\User\Vehicle\VehicleRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Exception;

class VehicleController extends Controller
{
    public function show(Request $request)
    {
        try {
            $vehicles = new VehicleRepo();
            $vehicles = $vehicles->getVehicle($request->all());

            return Response::json([
                'success' => true,
                'message' => $vehicles ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $vehicles,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {
            $vehicles = new VehicleRepo();
            $vehicles = $vehicles->getVehicleByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => count($vehicles) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $vehicles,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(StoreVehicleRequest $request)
    {
        try {
            $vehicles = new VehicleRepo();
            $vehicles->store($request->all());

            return Response::json([
                'success' => true,
                'message' => $request->get("vehicle_id") ? Lang::get('vehicle.create_vehicle.edit_success') : Lang::get('vehicle.create_vehicle.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('vehicle_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $vehicles = new VehicleRepo();
            $vehicles->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('vehicle.create_vehicle.edit_success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function putMain(Request $request)
    {
        try {
            if (!$request->get('vehicle_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $vehicles = new VehicleRepo();
            $vehicles->putMain($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('vehicle.create_vehicle.put_main'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(Request $request)
    {
        try {
            if (!$request->get('vehicle_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $vehicles = new VehicleRepo();
            $vehicles->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('vehicle.create_vehicle.delete_success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function storeFeatures(Request $request)
    {
        try {

            if ($request->get('vehicle_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $vehicles = new VehicleRepo();
            $vehicles->storeFeatures($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function updateFeatures(Request $request)
    {
        try {
            if (!$request->get('features_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $vehicles = new VehicleRepo();
            $vehicles->storeFeatures($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allFeatures(Request $request)
    {
        try {
            $allFeatures = new VehicleRepo();
            $allFeatures = $allFeatures->allFeatures($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success'),
                'data' => $allFeatures
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
