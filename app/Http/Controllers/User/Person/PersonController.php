<?php

namespace App\Http\Controllers\User\Person;

use App\Http\Controllers\Controller;
use App\Repositories\User\Person\PersonRepo;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class PersonController extends Controller
{
    public function show(Request $request)
    {
        try {

            $person = new PersonRepo();
            $person = $person->getPerson($request->all());

            return Response::json([
                'success' => true,
                'message' => count($person) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $person,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {
            if ($request->get('user_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $person = new PersonRepo();
            $person = $person->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_group.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('user_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $person = new PersonRepo();
            $person = $person->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_group.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
