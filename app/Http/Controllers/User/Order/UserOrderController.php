<?php

namespace App\Http\Controllers\User\Order;

use App\Http\Controllers\Controller;
use App\Repositories\User\Order\UserOrderRepo;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class UserOrderController extends Controller
{


    public function show(Request $request)
    {
        try {
            $orders = new UserOrderRepo();
            $orders = $orders->getOrder($request->all());

            return Response::json([
                'success' => true,
                'message' => $orders ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $orders,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {
            $orders = new UserOrderRepo();
            $orders = $orders->getOrderByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => count($orders) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $orders,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {
            if ($request->get('order_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $orders = new UserOrderRepo();
            $orders = $orders->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('order.create.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function cancel(Request $request)
    {
        try {
            $orders = new UserOrderRepo();
            $orders = $orders->cancel($request->all());

            return Response::json([
                'success' => $orders['success'],
                'message' => $orders['success'] ? Lang::get('order.create.cancel') : Lang::get('order.create.error_cancel'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allCompany(Request $request)
    {
        try {
            $orders = new UserOrderRepo();
            $orders = $orders->getAllCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => $orders ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $orders,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allService(Request $request)
    {
        try {
            $services = new UserOrderRepo();
            $services = $services->getAllService($request->all());

            return Response::json([
                'success' => true,
                'message' => $services ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $services,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allPayments(Request $request)
    {
        try {
            $payments = new UserOrderRepo();
            $payments = $payments->getAllPayments($request->all());

            return Response::json([
                'success' => true,
                'message' => $payments ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $payments,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allOrderStatus(Request $request)
    {
        try {
            $status = new UserOrderRepo();
            $status = $status->getAllOrderStatus($request->all());

            return Response::json([
                'success' => true,
                'message' => $status ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $status,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function serviceByVehicle(Request $request)
    {
        try {
            $services = new UserOrderRepo();
            $services = $services->getServiceByVehicle($request->all());

            return Response::json([
                'success' => true,
                'message' => $services ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $services,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function checkCoupon(Request $request)
    {
        try {
            $coupon = new UserOrderRepo();
            $coupon = $coupon->checkCoupon($request->all());

            return Response::json([
                'success' => true,
                'data' => $coupon,
                'message' => Lang::get('order.found_coupon'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
