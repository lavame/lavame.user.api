<?php

namespace App\Http\Controllers\User\Address;

use App\Http\Controllers\Controller;
use App\Http\Requests\Address\StoreAddressRequest;
use App\Repositories\User\Address\AddressRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Exception;

class AddressController extends Controller
{
    public function show(Request $request)
    {
        try {
            $address = new AddressRepo();
            $address = $address->getAddress($request->all());

            return Response::json([
                'success' => true,
                'message' => $address ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $address,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showCompany(Request $request)
    {
        try {
            $address = new AddressRepo();
            $address = $address->getCompanyAddress($request->all());

            return Response::json([
                'success' => true,
                'message' => count($address) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $address,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {
            $address = new AddressRepo();
            $address = $address->getAddressByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => $address ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $address,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(StoreAddressRequest $request)
    {
        try {
            if ($request->get('address_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $address = new AddressRepo();
            $address->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('address.create_address.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('address_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $address = new AddressRepo();
            $address->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('address.create_address.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(Request $request)
    {
        try {
            if (!$request->get('address_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $address = new AddressRepo();
            $address->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allFeatures()
    {
        try {

            $allFeatures = new AddressRepo();
            $allFeatures = $allFeatures->allFeatures();

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success'),
                'data' => $allFeatures
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
