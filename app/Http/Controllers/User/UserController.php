<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUserRequest;
use App\Repositories\User\UserRepo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Lang;

class UserController extends Controller
{

    public function show(Request $request)
    {
        try {
            $userRepo = new UserRepo();
            $user = $userRepo->getUserInformation($request->all());

            return Response::json([
                'success' => true,
                'message' => $user ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $user,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(StoreUserRequest $request)
    {
        try {
            $userRepo = new UserRepo();
            $userRepo->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function remember(Request $request)
    {
        try {
            $userRepo = new UserRepo();
            $userRepo->remember($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('user.user_remember'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('user_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $userRepo = new UserRepo();
            $userRepo->update($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function delete(Request $request)
    {
        try {
            if (!$request->get('user_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $userRepo = new UserRepo();
            $userRepo->delete($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function checkLoginOrEmail(Request $request)
    {
        try {
            $userRepo = new UserRepo();
            $userRepo = $userRepo->checkLoginOrEmail($request->all())->exists();

            if ($userRepo) {
                throw new Exception(Lang::get('user.create_user.login_email_exists'), 409);
            }

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
