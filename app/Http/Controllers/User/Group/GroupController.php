<?php

namespace App\Http\Controllers\User\Group;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Repositories\User\Group\GroupRepo;
use App\Repositories\User\UserRepo;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class GroupController extends Controller
{
    public function show(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
                throw new Exception(Lang::get('common.permission'), 200);

            $group = new GroupRepo();
            $group = $group->getGroup($request->all());

            return Response::json([
                'success' => true,
                'message' => count($group) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $group,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || !$request->get('user_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $group = new GroupRepo();
            $group = $group->getGroupByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => count($group) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $group,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || $request->get('group_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $group = new GroupRepo();
            $group->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('group.create_group.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || !$request->get('group_id'))
                throw new Exception(Lang::get('common.permission'), 200);


            $group = new GroupRepo();
            $group->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('group.create_group.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function storeByUser(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || !$request->get('group_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $group = new GroupRepo();
            $group->storeByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(Request $request)
    {
        try {
            if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
                throw new Exception(Lang::get('common.permission'), 200);

            $group = new GroupRepo();
            $group->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
