<?php

namespace App\Http\Controllers\User\Coupon;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupon\StoreCouponRequest;
use App\Http\Requests\Coupon\StoreCouponUserRequest;
use App\Repositories\User\Coupon\CouponRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Exception;

class CouponController extends Controller
{
    public function show(Request $request)
    {
        try {

            $coupons = new CouponRepo();
            $coupons = $coupons->getCoupon($request->all());

            return Response::json([
                'success' => true,
                'message' => count($coupons) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $coupons,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {

            $coupons = new CouponRepo();
            $coupons = $coupons->getCouponByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => count($coupons) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $coupons,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(StoreCouponRequest $request)
    {
        try {
            if ($request->get('coupon_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $coupons = new CouponRepo();
            $coupons->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('coupon.create_coupon.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('coupon_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $coupons = new CouponRepo();
            $coupons->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('coupon.create_coupon.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function storeByUser(StoreCouponUserRequest $request)
    {
        try {

            $coupons = new CouponRepo();
            $coupons->storeByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(StoreCouponUserRequest $request)
    {
        try {
            $coupons = new CouponRepo();
            $coupons->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    //criar cupom de maneira dinamica depedendo da coluna passada - fazer na hora do front, assim tenho ideia de como vai ser
}
