<?php

namespace App\Http\Controllers\Company\Order;

use App\Http\Controllers\Controller;
use App\Repositories\Company\Order\CompanyOrderRepo;
use App\Repositories\User\Order\UserOrderRepo;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class CompanyOrderController extends Controller
{
    public function showByCompany(Request $request)
    {
        try {
            $orders = new CompanyOrderRepo();
            $orders = $orders->getOrderByCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => count($orders) ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $orders,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() :  400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('order_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $orders = new CompanyOrderRepo();
            $orders = $orders->update($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('order.create.update'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function addValueDirt(Request $request)
    {
        try {
            if (!$request->get('order_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $orders = new CompanyOrderRepo();
            $orders = $orders->addValueDirt($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('order.dirt'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
