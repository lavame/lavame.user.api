<?php

namespace App\Http\Controllers\Company\Bank;

use App\Http\Controllers\Controller;
use App\Repositories\Company\Bank\CompanyBankRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Exception;

class CompanyBankController extends Controller
{
    public function show(Request $request)
    {
        try {
            $bank = new CompanyBankRepo();
            $bank = $bank->getBank($request->all());

            return Response::json([
                'success' => true,
                'message' => $bank ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $bank,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $address = new CompanyBankRepo();
            $address->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('bank.create_bank.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function withdrawMoney(Request $request)
    {
        try {
            $bank = new CompanyBankRepo();
            $bank->withdrawMoney($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success'),
                'data'    => '$bank',
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
