<?php

namespace App\Http\Controllers\Company\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\StoreCompanyRequest;
use App\Repositories\Company\CompanyRepo;
use App\Repositories\Company\Service\CompanyServiceRepo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Lang;

class CompanyServiceController extends Controller
{

    public function show(Request $request)
    {
        try {
            $services = new CompanyServiceRepo();
            $services = $services->show($request->all());

            return Response::json([
                'success' => true,
                'message' => $services ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $services,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode(), ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByCompany(Request $request)
    {
        try {
            $services = new CompanyServiceRepo();
            $services = $services->getServiceByCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => $services ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $services,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode(), ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {

            $companyRepo = new CompanyServiceRepo();
            $companyRepo->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByCompany(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $companyRepo = new CompanyServiceRepo();
            $companyRepo->deleteByCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('service.delete'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode(), ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allFeatures(Request $request)
    {
        try {
            $companyRepo = new CompanyServiceRepo();
            $companyRepo = $companyRepo->allFeatures($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success'),
                'data' => $companyRepo
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
