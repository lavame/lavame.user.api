<?php

namespace App\Http\Controllers\Company\Address;

use App\Http\Controllers\Controller;
use App\Repositories\Company\Address\CompanyAddressRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;
use Exception;

class CompanyAddressController extends Controller
{
    public function show(Request $request)
    {
        try {
            $address = new CompanyAddressRepo();
            $address = $address->getAddress($request->all());

            return Response::json([
                'success' => true,
                'message' => $address ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $address,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $address = new CompanyAddressRepo();
            $address->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('address.create_address.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(Request $request)
    {
        try {
            if (!$request->get('address_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            app()->make(AddressRepo::class)->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success')
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function allFeatures(Request $request)
    {
        try {
            $allFeatures = app()->make(AddressRepo::class)->allFeatures();

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.success'),
                'data' => $allFeatures
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], $e->getCode() == 200 ? $e->getCode() : 400, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
