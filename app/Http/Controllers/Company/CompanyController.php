<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\StoreCompanyRequest;
use App\Repositories\Company\CompanyRepo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Lang;

class CompanyController extends Controller
{
    public function checkUserByCompany(Request $request)
    {
        try {
            $companyRepo = new CompanyRepo();
            $companyRepo->checkUserByCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => '',
                'data'    => '',
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function show(Request $request)
    {
        try {

            $companyRepo = new CompanyRepo();
            $company = $companyRepo->getCompanyInformation($request->all());

            return Response::json([
                'success' => true,
                'message' => $company ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $company,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function showByUser(Request $request)
    {
        try {
            $companyRepo = new CompanyRepo();
            $companies = $companyRepo->getCompanyByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => $companies ? Lang::get('common.success') : Lang::get('common.error_show'),
                'data'    => $companies,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function store(StoreCompanyRequest $request)
    {
        try {
            $companyRepo = new CompanyRepo();
            $companyRepo->store($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function update(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $companyRepo = new CompanyRepo();
            $companyRepo->update($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('common.create_user.edit_success'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function deleteByUser(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $companyRepo = new CompanyRepo();
            $companyRepo->deleteByUser($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('company.delete'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function activeCompany(Request $request)
    {
        try {
            if (!$request->get('company_id'))
                throw new Exception(Lang::get('common.permission'), 200);

            $companyRepo = new CompanyRepo();
            $companyRepo->activeCompany($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('company.active_company'),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
