<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Repositories\Dashboard\DashboardRepo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Lang;

class DashboardController extends Controller
{
    public function userDashboard(Request $request)
    {
        try {
            $dashboard = new DashboardRepo();
            $dashboard = $dashboard->getUserDashboard($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('user.dashboard'),
                'data'    => $dashboard,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function companyDashboard(Request $request)
    {
        try {
            $dashboard = new DashboardRepo();
            $dashboard = $dashboard->getCompanyDashboard($request->all());

            return Response::json([
                'success' => true,
                'message' => Lang::get('user.dashboard'),
                'data'    => $dashboard,
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            return Response::json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
