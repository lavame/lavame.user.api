<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Response;

class LoginController extends Controller
{
    public function login()
    {
        $credentials = request(['login', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return Response::json([
                'success' => false,
                'message' => Lang::get('user.incorrect_login'),
            ], 401, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        return response()->json(['success' => true, 'token' => $this->respondWithToken($token)]);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['success' => true, 'message' => 'Sucesso']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
