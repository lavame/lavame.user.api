<?php

namespace App\Repositories\Dashboard;

use App\Helpers\Constants;
use App\Models\Order\Order;
use App\Models\Vehicle\VehicleUser;
use App\Repositories\Company\CompanyRepo;
use App\Repositories\User\UserRepo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DashboardRepo
{
    public function getUserDashboard(array $options = null)
    {

        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $mainVehicle = VehicleUser::query()
            ->select('v.name')
            ->from('user.list_vehicle as lv')
            ->join('user.vehicle as v', 'lv.vehicle_id', '=', 'v.id')
            ->where('main', '=', true)
            ->where('user_id', '=', $options['user_id'] ?? auth()->user()->id)->first();

        $countVehicles = VehicleUser::query()
            ->where('user_id', '=', $options['user_id'] ?? auth()->user()->id)->count();

        $maxCountVehiclesOrder = Order::query()
            ->selectRaw("count(*) as amount, v.name")
            ->join('user.vehicle as v', 'company.order.vehicle_id', '=', 'v.id')
            ->where('user_id', '=', $options['user_id'] ?? auth()->user()->id)
            ->groupBy(['v.id'])
            ->having(DB::raw('count(*)'), '=', DB::raw('(select max(cnt)
                                                            from (select count(*) as cnt
                                                            from company.order as o
                                                            inner join "user"."vehicle" as vv on vv.id = o.vehicle_id
                                                            where o.user_id::text = ' . "'" . $options['user_id'] . "'" . ' group by vv.id)x)'))
            ->first();

        $maxCountCompanyOrder = Order::query()
            ->selectRaw("count(*) as amount, c.name")
            ->join('company.company as c', 'company.order.company_id', '=', 'c.id')
            ->where('user_id', '=', $options['user_id'] ?? auth()->user()->id)
            ->groupBy(['c.id'])
            ->having(DB::raw('count(*)'), '=', DB::raw('(select max(cnt)
                                                            from (select count(*) as cnt
                                                            from company.order o
                                                            inner join company.company as cc on cc.id = o.company_id
                                                            where o.user_id = ' . "'" . $options['user_id'] . "'" . '
                                                            group by cc.id)x)'))
            ->first();

        $countOrder = Order::query()->where('user_id', '=', $options['user_id'])->count();

        return [
            'mainVehicle' => $mainVehicle,
            'countVehicles' => $countVehicles,
            'maxCountVehiclesOrder' => $maxCountVehiclesOrder,
            'maxCountCompanyOrder' => $maxCountCompanyOrder,
            'countOrder' => $countOrder
        ];
    }

    public function getCompanyDashboard(array $options = null)
    {
        app()->make(CompanyRepo::class)->checkUserByCompany($options);

        $maxCountCompanyOrder = Order::query()
            ->selectRaw("count(*) as amount")
            ->where('company_id', '=', Arr::get($options, 'company_id'))->first();

        $maxCountServiceOrder = Order::query()
            ->selectRaw("count(*) as amount, s.name")
            ->join('company.list_service_modality as lsm', 'company.order.service_modality_id', '=', 'lsm.id')
            ->join('company.service as s', 'lsm.service_id', '=', 's.id')
            ->where('company.order.company_id', '=', Arr::get($options, 'company_id'))
            ->groupBy(['lsm.service_id', 's.name'])
            ->having(DB::raw('count(*)'), '=', DB::raw('(select max(cnt)
                                                            from (select count(*) as cnt
                                                            from company.order oo
                                                            inner join company.list_service_modality as slsm on oo.service_modality_id = slsm.id
                                                            where oo.company_id = ' . "'" . Arr::get($options, 'company_id') . "'" . '
                                                            group by slsm.service_id)x)'))
            ->first();




        $maxCountModalityOrder = Order::query()
            ->selectRaw("count(*) as amount, vm.name")
            ->join('company.list_service_modality as lsm', 'company.order.service_modality_id', '=', 'lsm.id')
            ->join('user.vehicle_modality as vm', 'lsm.modality_id', '=', 'vm.id')
            ->where('company.order.company_id', '=', Arr::get($options, 'company_id'))
            ->groupBy(['lsm.modality_id', 'vm.name'])
            ->having(DB::raw('count(*)'), '=', DB::raw('(select max(cnt)
                                                            from (select count(*) as cnt
                                                            from company.order oo
                                                            inner join company.list_service_modality as slsm on oo.service_modality_id = slsm.id
                                                            where oo.company_id = ' . "'" . Arr::get($options, 'company_id') . "'" . '
                                                            group by slsm.modality_id)x)'))
            ->first();


        $maxCountEarnings = Order::query()
            ->selectRaw('sum(p.value_company) as value_total')
            ->from('company.order as o')
            ->join('company.payment as p', 'o.payment_id', '=', 'p.id')
            ->where('o.company_id', '=', Arr::get($options, 'company_id'))
            ->where('p.received_by_company', '=', true)->first();

        return [
            'maxCountCompanyOrder' => $maxCountCompanyOrder,
            'maxCountServiceOrder' => $maxCountServiceOrder,
            'maxCountModalityOrder' => $maxCountModalityOrder,
            'maxCountEarnings' => $maxCountEarnings,
        ];
    }
}
