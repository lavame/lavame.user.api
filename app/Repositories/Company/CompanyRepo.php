<?php

namespace App\Repositories\Company;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Company\Company;
use App\Models\Company\CompanyUser;
use App\Repositories\User\UserRepo;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CompanyRepo
{
    public function getCompanyInformation(array $options = null)
    {
        $this->checkUserByCompany($options);

        $data = Company::query()
            ->with(Arr::get($options, 'relations') ?? []);

        if (!Arr::get($options, 'company_all')) {
            $data->where('id', Arr::get($options, 'company_id'));
        }

        return Arr::get($options, 'company_all') ? $data->get() : $data->first();
    }

    public function getCompanyByUser(array $options = null)
    {
        $userRepo = new UserRepo();
        $userRepo->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $data = CompanyUser::query()
            ->with(Arr::get($options, 'relations') ?? [])
            ->where('status', '=', true);

        if (!$userRepo->checkPermissionGroup([Constants::GROUP_ADMIN]) || Arr::get($options, 'user_id'))
            $data->where('company.list_company_user.user_id', Arr::get($options, 'user_id') ?? auth()->user()->id);

        return $data->get();
    }

    public function store(array $options)
    {
        $exists = Company::query()
            ->where(function ($query) use ($options) {
                $query->orWhere('company.company.email', '=', Arr::get($options, 'company_email'))
                    ->orWhere('company.company.cnpj', '=', Arr::get($options, 'company_cnpj'));
            })->exists();

        if ($exists) {
            throw new Exception(Lang::get('company.create_company.login_email_exists'), 409);
        }

        $company = new Company();
        $company->id = Str::uuid();

        DB::beginTransaction();
        $company->name = Arr::get($options, 'company_name');
        $company->cnpj = Arr::get($options, 'company_cnpj');
        $company->email = Arr::get($options, 'company_email');
        $company->closing_time = explode('T', Arr::get($options, 'company_closing_time'))[1];
        $company->opening_time = explode('T', Arr::get($options, 'company_opening_time'))[1];
        $company->phone = Arr::get($options, 'company_phone');

        $company->save();

        $companyUser = new CompanyUser();
        $companyUser->id = Str::uuid();
        $companyUser->user_id = Arr::get($options, 'user_id') ?? auth()->user()->id;
        $companyUser->company_id = $company->id;

        $companyUser->save();

        Util::sendEmailUser([
            'user_name' => $company->name,
            'user_email' => $company->email,
            'text' => 'Cadastro feito com sucesso, Bem vindo ao LAVAME',
            'subject' => 'Cadastro concluído',
        ]);

        DB::commit();
    }

    public function update(array $options)
    {
        $company = Company::query()
            ->where(function ($query) use ($options) {
                $query->orWhere('company.company.email', '=', Arr::get($options, 'company_email'))
                    ->orWhere('company.company.cnpj', '=', Arr::get($options, 'company_cnpj'));
            })->get();

        if (count($company) > 1) {
            throw new Exception(Lang::get('user.create_user.login_email_exists'), 409);
        }

        if (!count($company)) {
            $company = Company::query()->where('id', '=', Arr::get($options, 'company_id'))->first();
        } else {
            $company = $company[0];
        }

        $company->name = Arr::get($options, 'company_name') ?? $company->name;
        $company->cnpj = Arr::get($options, 'company_cnpj') ?? $company->cnpj;
        $company->email = Arr::get($options, 'company_email') ?? $company->email;
        $company->closing_time = explode('T', Arr::get($options, 'company_closing_time'))[1] ?? $company->closing_time;
        $company->opening_time = explode('T', Arr::get($options, 'company_opening_time'))[1] ?? $company->opening_time;
        $company->phone = Arr::get($options, 'company_phone') ?? $company->phone;
        $company->save();

        Util::sendEmailUser([
            'user_name' => $company->name,
            'user_email' => $company->email,
            'text' => 'Editado com sucesso, Bem vindo ao LAVAME',
            'subject' => 'Edição concluído',
        ]);
    }

    public function activeCompany(array $options)
    {

        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $companyUser = CompanyUser::query()->where('company_id', '=', Arr::get($options, 'company_id'))->first();
        $companyUser->active = true;
        $companyUser->date_active = Carbon::now();

        $companyUser->save();
        $company = Company::query()->where('id', '=', Arr::get($options, 'company_id'))->first();
        $company->commission = 10;
        $company->save();

        Util::sendEmailUser([
            'text' => 'Ativação foi realizada com sucesso, Bem vindo ao LAVAME',
            'subject' => 'Ativação concluída',
            'bcc' => $company->email
        ]);
    }

    public function deleteByUser(array $options)
    {
        $company = CompanyUser::query()->where('company.list_company_user.company_id', '=', Arr::get($options, 'company_id'))->first();
        $company->status = false;
        $company->save();
    }

    public function checkUserByCompany(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            if (!Arr::get($options, 'company_id'))
                throw new Exception(Lang::get('common.permission'));

            $companyList = CompanyUser::query()
                ->where('user_id', '=',  Arr::get($options, 'user_id') ?? auth()->user()->id)
                ->where('company_id', '=',  Arr::get($options, 'company_id'))->exists();

            if (!$companyList)
                throw new Exception(Lang::get('common.permission'));
        }
    }
}
