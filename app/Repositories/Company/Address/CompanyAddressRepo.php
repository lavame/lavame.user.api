<?php

namespace App\Repositories\Company\Address;

use App\Helpers\Constants;
use App\Models\Address\Address;
use App\Models\Address\City;
use App\Models\Address\ListAddress;
use App\Models\Address\State;
use App\Models\Company\Company;
use App\Models\Company\CompanyUser;
use App\Repositories\Company\CompanyRepo;
use App\Repositories\User\UserRepo;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CompanyAddressRepo
{
    public function getAddress(array $options = null)
    {
        $companyRepo = new CompanyRepo();
        $companyRepo->checkUserByCompany($options);

        $company = Company::query()->where('id', '=',  Arr::get($options, 'company_id'))->first();

        $data = Address::query()
            ->with(Arr::get($options, 'relations') ?? ['city', 'state'])
            ->where('id', '=', $company->address_id);

        return $data->first();
    }

    public function store(array $options = null)
    {
        DB::beginTransaction();

        $company = Company::query()->where('id', '=', Arr::get($options, 'company_id'))->first();
        $address = '';

        if ($company->address_id) {
            $address = Address::query()
                ->where('id', '=', $company->address_id)
                ->first();
        } else {
            $address = new Address();
            $address->id = Str::uuid();
            $company->address_id = $address->id;
        }

        $company->latitude = Arr::get($options, 'address_lat')   ?? $address->latitude;
        $company->longitude = Arr::get($options, 'address_lng')   ?? $address->longitude;

        $address->zip_code   = Arr::get($options, 'address_zip_code')   ?? $address->zip_code;
        $address->number     = Arr::get($options, 'address_number')     ?? $address->number;
        $address->street     = Arr::get($options, 'address_street')     ?? $address->street;
        $address->complement = Arr::get($options, 'address_complement') ?? $address->complement;
        $address->city_id    = Arr::get($options, 'address_city_id')    ?? $address->city_id;
        $address->state_id   = Arr::get($options, 'address_state_id')   ?? $address->state_id;
        $address->save();
        $company->save();
        DB::commit();
    }
}
