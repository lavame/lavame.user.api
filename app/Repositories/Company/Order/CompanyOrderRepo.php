<?php

namespace App\Repositories\Company\Order;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Company\Company;
use App\Models\Company\ServiceModality;
use App\Models\Order\Order;
use App\Models\Order\OrderStatus;
use App\Models\Payment\Payment;
use App\Models\Payment\PaymentStatus;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class CompanyOrderRepo
{
    public function getOrderByCompany(array $options = null)
    {
        $data = Order::query()
            ->selectRaw("o.id, TO_CHAR(o.date, 'DD/MM/YYYY HH24:MI:SS') as date, o.start_date, o.final_date, c.name as company, ps.name as type_name, os.name as status_name, ps.id as type, os.id as status")
            ->from('company.order as o')
            ->join('company.list_service_modality as sm', 'o.service_modality_id', '=', 'sm.id')
            ->join('company.service as s', 'sm.service_id', '=', 's.id')
            ->join('company.order_status as os', 'o.status_id', '=', 'os.id')
            ->join('company.company as c', 'o.company_id', '=', 'c.id')
            ->join('company.payment as p', 'o.payment_id', '=', 'p.id')
            ->join('company.payment_status as ps', 'p.status_id', '=', 'ps.id')
            ->where('o.company_id', '=', Arr::get($options, 'company_id'))
            ->orderBy('date', 'DESC');

        return $data->get();
    }

    public function update(array $options = null)
    {

        DB::beginTransaction();

        $this->checkOrderByCompany($options);

        $order = Order::query()
            ->where('company.order.id', '=', $options['order_id'])->first();
        $payment = Payment::query()
            ->where('id', '=', $order->payment_id)->first();

        if ($options['order_status'] === Constants::ORDER_STATUS_ACCEPTED && $order->status_id === Constants::ORDER_STATUS_DONE) {
            $order->accepted_date = Carbon::now()->format('Y-m-d H:i:s');
            if ($order->scheduling) $options['order_status'] = Constants::ORDER_STATUS_SCHEDULED;
        } else if (($options['order_status'] === Constants::ORDER_STATUS_REFUSED || $options['order_status'] === Constants::ORDER_STATUS_CLOSED)
            && !$order->status_id === Constants::ORDER_STATUS_CLOSED
        ) {
            $order->received_by_company = false;
            $payment->status_id = Constants::PAYMENT_STATUS_CANCELED;
        } else if ($options['order_status'] === Constants::ORDER_STATUS_PROGRESS) {
            $order->start_date = Carbon::now()->format('Y-m-d H:i:s');
        } else if ($options['order_status'] === Constants::ORDER_STATUS_FINISHED) {
            $order->final_date = Carbon::now()->format('Y-m-d H:i:s');
        } else if ($options['order_status'] === Constants::ORDER_STATUS_CLOSED) {
            $payment->status_id = Constants::PAYMENT_STATUS_APPROVED;
        } else if (
            $options['order_status'] === Constants::ORDER_STATUS_CANCELED &&
            (in_array($order->status_id, [Constants::ORDER_STATUS_ACCEPTED, Constants::ORDER_STATUS_SCHEDULED, Constants::ORDER_STATUS_PROGRESS]))
        ) {
            $payment->status_id = Constants::PAYMENT_STATUS_CANCELED;
        } else {
            throw new Exception(Lang::get('common.permission'), 200);
        }

        $order->status_id = $options['order_status'];
        $order->save();
        $payment->save();

        $company = Company::query()->where('id', '=', $order->company_id)->first();
        $orderStatus = OrderStatus::query()->where('id', '=', $options['order_status'])->first();
        $paymentStatus = PaymentStatus::query()->where('id', '=', $payment->status_id)->first();

        Util::sendEmailUser([
            'text' => 'Seu pedido encontra-se no status: ' . $orderStatus->name .
                ' e o seu pagamento encontra-se no status: ' . $paymentStatus->name . ' , Obrigado por fazer parte do LAVAME',
            'subject' => 'Pedido '  . $orderStatus->name . ' com sucesso',
            'bcc' => $company->email
        ]);

        DB::commit();
    }

    public function addValueDirt(array $options = null)
    {
        $this->checkOrderByCompany($options);
        $order = Order::query()->where('id', '=', Arr::get($options, 'order_id'))->first();
        $serviceModality = ServiceModality::query()->where('id', '=', $order->service_modality_id)->first();
        $payment = Payment::query()->where('id', '=', $order->payment->id)->first();

        $payment->value_company += $serviceModality->value_dirt;
        $payment->check_value_dirt = true;
        $payment->save();

        $company = Company::query()->where('id', '=', $order->company_id)->first();

        Util::sendEmailUser([
            'text' => 'Uma taxa adicionou foi cobrada no valor de R$:' . $serviceModality->value_dirt . ' reais, obrigado por fazer parte do LAVAME',
            'subject' => 'Taxa adicional',
            'bcc' => $company->email
        ]);
    }

    private function checkOrderByCompany(array $options = null)
    {
        $checkUser = new UserRepo();

        if (!$checkUser->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            $checkUser->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

            $checkOrderByUser = Order::query()
                ->join('company.list_company_user as lcu', 'company.order.company_id', '=', 'lcu.company_id')
                ->where('company.order.id', Arr::get($options, 'order_id'))
                ->where('company.order.company_id', Arr::get($options, 'company_id'))
                ->where('lcu.user_id', '=', Arr::get($options, 'user_id') ?? auth()->user()->id)->exists();

            if (!$checkOrderByUser)
                throw new Exception(Lang::get('common.permission'), 200);
        }
    }
}
