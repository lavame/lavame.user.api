<?php

namespace App\Repositories\Company\Bank;

use App\Helpers\Constants;
use App\Models\Address\Address;
use App\Models\Address\City;
use App\Models\Address\ListAddress;
use App\Models\Address\State;
use App\Models\Bank\Bank;
use App\Models\Company\Company;
use App\Models\Company\CompanyUser;
use App\Models\Order\Order;
use App\Models\Payment\Payment;
use App\Repositories\User\UserRepo;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CompanyBankRepo
{
    //fazer verificacao
    public function getBank(array $options = null)
    {
        $company = Company::query()->where('id', '=',  Arr::get($options, 'company_id'))->first();

        $data = Bank::query()
            ->where('id', '=', $company->bank_id)->first();

        $totalToWithdraw = $this->totalToWithdraw($options);

        return [
            'bank' => $data,
            'totalToWithdraw' => $totalToWithdraw,
        ];
    }

    public function store(array $options = null)
    {
        DB::beginTransaction();

        $company = Company::query()->where('id', '=', Arr::get($options, 'company_id'))->first();
        $bank = '';


        if ($company->bank_id) {
            $bank = Bank::query()
                ->where('id', '=', $company->bank_id)
                ->first();
        } else {
            $bank = new Bank();
            $bank->id = Str::uuid();
            $company->bank_id = $bank->id;
        }

        $bank->name = Arr::get($options, 'bank_name') ?? $bank->name;
        $bank->agency = Arr::get($options, 'bank_agency') ?? $bank->agency;
        $bank->account = Arr::get($options, 'bank_account') ?? $bank->account;
        $bank->save();
        $company->save();
        DB::commit();
    }

    public function withdrawMoney(array $options = null)
    {
        $orders = Order::query()
            ->select('payment_id')
            ->where('company_id', '=', Arr::get($options, 'company_id'))
            ->where('status_id', '=', Constants::ORDER_STATUS_CLOSED)->get();

        foreach ($orders as $order) {
            $payment = Payment::query()->where('id', '=', $order->payment_id)->first();
            if (!$payment->received_by_company) {
                $payment->received_by_company = true;
                $payment->save();
            }
        }
    }

    private function totalToWithdraw(array $options = null)
    {
        return Order::query()
            ->from('company.order as o')
            ->selectRaw('sum(p.value_company) as value_total')
            ->from('company.order as o')
            ->join('company.payment as p', 'o.payment_id', '=', 'p.id')
            ->where('o.company_id', '=', Arr::get($options, 'company_id'))
            ->where('p.received_by_company', '=', false)
            ->where('p.status_id', Constants::PAYMENT_STATUS_APPROVED)
            ->where('o.status_id', '=', Constants::ORDER_STATUS_CLOSED)->first();
    }
}
