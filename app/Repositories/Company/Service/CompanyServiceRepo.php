<?php

namespace App\Repositories\Company\Service;

use App\Helpers\Constants;
use App\Models\Company\Company;
use App\Models\Company\CompanyService;
use App\Models\Company\CompanyUser;
use App\Models\Company\Service;
use App\Models\Company\ServiceModality;
use App\Models\User\User;
use App\Models\Vehicle\VehicleModality;
use App\Repositories\Company\CompanyRepo;
use App\Repositories\User\Address\AddressRepo;
use App\Repositories\User\UserRepo;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;


class CompanyServiceRepo
{
    public function show(array $options = null)
    {
        $service = Service::query()
            ->where('id', '=', Arr::get($options, 'service_id'))->first();

        $serviceValues = ServiceModality::query()
            ->with(Arr::get($options, 'relations') ?? ['modality'])
            ->where('service_id', '=', Arr::get($options, 'service_id'))->get();

        return [
            "service" => $service,
            "serviceValues" => $serviceValues,
        ];
    }

    public function getServiceByCompany(array $options = null)
    {
        $companyRepo = new CompanyRepo();
        $companyRepo->checkUserByCompany($options);

        return CompanyService::query()
            ->with(Arr::get($options, 'relations') ?? ['service'])
            ->where('company.list_company_service.company_id', '=',  Arr::get($options, 'company_id'))
            ->where('active', '=', true)->get();
    }

    public function store($options)
    {
        DB::beginTransaction();
        $service = '';

        if (!isset($options['service_id'])) {

            $service = new Service();
            $service->id = Str::uuid();
            $service->name = Arr::get($options, 'service')['name'];
            $service->description = Arr::get($options, 'service')['description'];
            $service->save();

            $serviceCompany = new CompanyService();
            $serviceCompany->id = Str::uuid();
            $serviceCompany->company_id = $options['company_id'];
            $serviceCompany->service_id =  $service->id;
            $serviceCompany->save();
        } else {
            $this->checkServiceByCompany($options);
        }

        foreach (Arr::get($options, 'serviceValues') as $value) {
            if (!isset($value['id'])) {
                $serviceModality = new ServiceModality();
                $serviceModality->id = Str::uuid();
                $serviceModality->service_id = $value['service_id'] ?? $service->id;
                $serviceModality->modality_id = $value['modality_id'];
            } else {
                $serviceModality = ServiceModality::query()
                    ->where('service_id', '=', $value['service_id'] ?? $service->id)
                    ->where('modality_id', '=',  $value['modality_id'])
                    ->first();
            }
            $serviceModality->value = $value['value'] ?? $serviceModality->value;
            $serviceModality->value_dirt = $value['value_dirt'] ?? $serviceModality->value_dirt;
            $serviceModality->save();
        }

        DB::commit();
    }

    public function deleteByCompany(array $options)
    {
        $service = CompanyService::query()
            ->where('company.list_company_service.company_id', '=', Arr::get($options, 'company_id'))
            ->where('company.list_company_service.service_id', '=', Arr::get($options, 'service_id'))->first();
        $service->active = false;
        $service->save();
    }

    public function allFeatures()
    {
        return VehicleModality::query()->get()->ToArray();
    }

    public function checkServiceByCompany(array $options)
    {
        //checar usuario
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            $companyList = CompanyService::query()
                ->where('service_id', '=',  Arr::get($options, 'service_id'))
                ->where('company_id', '=',  Arr::get($options, 'company_id'))->first();


            if (!$companyList)
                throw new Exception(Lang::get('common.permission'));
        }
    }
}
