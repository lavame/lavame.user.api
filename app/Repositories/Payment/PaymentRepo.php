<?php

namespace App\Repositories\Payment;

use App\Helpers\Constants;
use App\Models\Bank\Bank;
use App\Models\Company\Company;
use App\Models\Order\Order;
use App\Models\Payment\Payment;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class PaymentRepo
{
    public function getOrder(array $options = null)
    {
        $this->checkOrderByUser($options);

        $data = Order::query()
            ->with(Arr::get($options, 'relations') ?? ['serviceModality', 'orderStatus', 'company', 'vehicle', 'payment']);

        if (Arr::get($options, 'id'))
            $data->where('id', Arr::get($options, 'vehicle_id'));

        return Arr::get($options, 'user_all') ? $data->get() : $data->first();
    }

    public function getOrderByUser(array $options = null)
    {
        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $data = Order::query()
            ->with(Arr::get($options, 'relations') ?? ['serviceModality', 'orderStatus', 'company', 'vehicle', 'payment'])
            ->where('order.user_id', Arr::get($options, 'user_id'));

        return $data->get();
    }

    public function store(array $options = null)
    {
        $company = Company::query()->where('id', '=', $options['company_id'])->first();

        $valuePercentage = (($options['value_total'] - $options['value_coupon']) * $company->commission) / 100;

        $payment = new Payment();
        $payment->id = Str::uuid();
        $payment->value_total = $options['value_total'];
        $payment->value_system = $valuePercentage;
        $payment->value_company = ($options['value_total'] - $options['value_coupon']) - $valuePercentage;
        $payment->type_id = $options['payment_type_id'];
        $payment->value_coupon = $options['value_coupon'];
        $payment->status_id = $options['payment_type_id'] === Constants::PAYMENT_TYPE_CARD ? Constants::PAYMENT_STATUS_APPROVED : Constants::PAYMENT_STATUS_WAITING;
        $payment->save();

        return $payment;
    }

    private function checkOrderByUser(array $options = null)
    {
        $checkUser = new UserRepo();

        if (!$checkUser->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            $checkUser->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

            $checkOrderByUser = Order::query()
                ->where('id', Arr::get($options, 'id'))
                ->where('user_id', Arr::get($options, 'user_id') ?? auth()->user()->id)->exists();

            if (!$checkOrderByUser)
                throw new Exception(Lang::get('common.permission'), 200);
        }
    }
}
