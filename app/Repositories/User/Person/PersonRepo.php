<?php

namespace App\Repositories\User\Person;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Person\Person;
use App\Repositories\User\UserRepo;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class PersonRepo
{
    public function getPerson(array $options)
    {
        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);
        return Person::query()
            ->join('user.user', 'person.id', '=', 'user.person_id')
            ->with(Arr::get($options, 'relations') ?? [])
            ->where('id', Arr::get($options, 'user_id'));
    }

    public function store(array $options)
    {
        $person     = new Person();
        $person->id = Str::uuid();

        $person->name       = Arr::get($options, 'user_name') ?? $person->name;
        $person->birth_date = Arr::get($options, 'user_birth_date') ?
            Util::formatDatePatternUSA(Arr::get($options, 'user_birth_date')) : $person->birth_date;
        $person->cnh        = Arr::get($options, 'user_cnh')   ?? $person->cnh;
        $person->cpf        = Arr::get($options, 'user_cpf')   ?? $person->cpf; //validar cpf com aniversario
        $person->email      = Arr::get($options, 'user_email') ?? $person->email;
        $person->phone      = Arr::get($options, 'user_phone') ?? $person->phone; //tirar a mascara do fone
        $person->sex_id     = Arr::get($options, 'user_sex')   ?? $person->sex_id;
        $person->save();

        return $person;
    }
}
