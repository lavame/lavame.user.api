<?php

namespace App\Repositories\User\Order;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Company\Company;
use App\Models\Company\ServiceModality;
use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponCompany;
use App\Models\Coupon\CouponUser;
use App\Models\Order\Order;
use App\Models\Order\OrderStatus;
use App\Models\Payment\Payment;
use App\Models\Payment\PaymentType;
use App\Models\Vehicle\Vehicle;
use App\Repositories\Payment\PaymentRepo;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class UserOrderRepo
{
    public function getAllCompany(array $options = null)
    {
        $data = Company::query()
            ->selectRaw(Arr::get($options, 'select') ?? 'latitude as lat, longitude as lng, c.name, c.id')
            ->from('company.company as c');

        $this->filterAllCompany($data, $options);

        return Arr::get($options, 'order_id') || (Arr::get($options, 'company_id') && !Arr::get($options, 'filter')) ? $data->first() : $data->get();
    }

    public function getAllService(array $options = null)
    {
        $services = ServiceModality::query()
            ->selectRaw('s.name')
            ->from('company.list_service_modality as lsm')
            ->join('company.service as s', 'lsm.service_id', '=', 's.id');

        if ((Arr::get($options, 'company_id'))) {
            $services->join('company.list_company_service as cs', 'lsm.service_id', '=', 'cs.service_id')
                ->where('cs.company_id', '=', Arr::get($options, 'company_id'));
        }

        $services->groupBy(['s.name']);

        return $services->get();
    }

    public function getAllPayments()
    {
        return PaymentType::query()->get();
    }

    public function getAllOrderStatus()
    {
        return OrderStatus::query()->get();
    }

    public function getServiceByVehicle(array $options = null)
    {
        $vehicle = Vehicle::query()->where('id', '=', Arr::get($options, 'vehicle_id'))->first();

        return ServiceModality::query()
            ->selectRaw('lsm.id as service_modality_id, s.name, lsm.value, lsm.value_dirt')
            ->from('company.list_service_modality as lsm')
            ->join('company.service as s', 'lsm.service_id', '=', 's.id')
            ->join('company.list_company_service as lcs', 'lsm.service_id', '=', 'lcs.service_id')
            ->where('lcs.company_id', '=', Arr::get($options, 'company_id'))
            ->where('lsm.modality_id', '=', $vehicle->modality_id)->get();
    }

    public function getOrder(array $options = null)
    {
        $this->checkOrderByUser($options);

        $data = Order::query()
            ->selectRaw('o.id as order_id, o.date, o.start_date, o.final_date, o.scheduling, os.id as order_status_id, os.name as order_status_name,
                lsm.id as service_modality_id, s.id as service_id, s.name as service_name, v.id as vehicle_id, v.name as vehicle_name,
                c.id as company_id, c.name as company_name, c.cnpj as company_cnpj, c.phone as company_phone, c.email as company_email,
                p.value_total, ps.id as payment_status_id, ps.name as payment_status_name, pt.id as payment_type_id, p.value_company,
                pt.name as payment_name, "user".coupon.code as coupon_code, vm.name as model_name, vmo.name as modality_name, lsm.value_dirt, p.check_value_dirt')
            ->from('company.order as o')
            ->join('company.order_status as os', 'o.status_id', '=', 'os.id')
            ->join('company.list_service_modality as lsm', 'o.service_modality_id', '=', 'lsm.id')
            ->join('company.service as s', 'lsm.service_id', '=', 's.id')
            ->join('company.company as c', 'o.company_id', '=', 'c.id')
            ->join('company.payment as p', 'o.payment_id', '=', 'p.id')
            ->join('user.vehicle as v', 'o.vehicle_id', '=', 'v.id')
            ->join('user.vehicle_model as vm', 'v.model_id', '=', 'vm.id')
            ->join('user.vehicle_modality as vmo', 'v.modality_id', '=', 'vmo.id')
            ->join('company.payment_status as ps', 'p.status_id', '=', 'ps.id')
            ->join('company.payment_type as pt', 'p.type_id', '=', 'pt.id')

            ->leftJoin('user.coupon', 'o.coupon_id', '=', DB::raw('"user"."coupon"."id"::text'));

        if (Arr::get($options, 'order_id'))
            $data->where('o.id', Arr::get($options, 'order_id'));

        return Arr::get($options, 'user_all') ? $data->get() : $data->first();
    }

    public function getOrderByUser(array $options = null)
    {

        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $data = Order::query()
            ->selectRaw("o.id, TO_CHAR(o.date, 'DD/MM/YYYY HH24:MI:SS') as date, o.start_date, o.final_date, c.name as company, ps.name as type_name, os.name as status_name, ps.id as type, os.id as status")
            ->from('company.order as o')
            ->join('company.list_service_modality as sm', 'o.service_modality_id', '=', 'sm.id')
            ->join('company.service as s', 'sm.service_id', '=', 's.id')
            ->join('company.order_status as os', 'o.status_id', '=', 'os.id')
            ->join('company.company as c', 'o.company_id', '=', 'c.id')
            ->join('company.payment as p', 'o.payment_id', '=', 'p.id')
            ->join('company.payment_status as ps', 'p.status_id', '=', 'ps.id')
            ->where('o.user_id', '=', Arr::get($options, 'user_id'))
            ->orderBy('date', 'DESC');

        return $data->get();
    }

    public function store(array $options = null)
    {
        DB::beginTransaction();

        $getCoupon = $this->checkCoupon($options);
        $options['value_coupon'] = $getCoupon->discount_value ?? 0;

        $payment = new PaymentRepo();
        $payment = $payment->store($options);

        $order = new Order();
        $order->id = Str::uuid();
        $order->user_id = Arr::get($options, 'user_id') ?? auth()->user()->id;
        $order->date = Carbon::now()->format('Y-m-d H:i:s');
        $order->scheduling = Arr::get($options, 'scheduling');
        $order->service_modality_id = $options['service_modality_id'];
        $order->status_id = Constants::ORDER_STATUS_DONE;
        $order->company_id = Arr::get($options, 'company_id');
        $order->vehicle_id = Arr::get($options, 'vehicle_id');
        $order->payment_id = $payment->id;
        $order->coupon_id = $getCoupon->id ?? '';
        $order->save();

        $company = Company::query()->where('id', '=', Arr::get($options, 'company_id'))->first();

        Util::sendEmailUser([
            'text' => 'Pedido feito com sucesso, Obrigado por fazer parte do LAVAME',
            'subject' => 'Pedido feito com sucesso',
            'bcc' => $company->email
        ]);

        DB::commit();
    }

    public function cancel(array $options = null)
    {
        $this->checkOrderByUser($options);
        $order = Order::query()->where('id', '=', $options['order_id'])->first();

        if (
            !in_array($order->status_id, [Constants::ORDER_STATUS_REFUSED, Constants::ORDER_STATUS_FINISHED, Constants::ORDER_STATUS_CANCELED])
            && !$order->final_date
        ) {
            DB::beginTransaction();

            $order->status_id = Constants::ORDER_STATUS_CANCELED;
            $order->save();

            $payment = Payment::query()->where('id', '=', $order->payment_id)->first();
            $payment->received_by_company = false;
            $payment->status_id = Constants::PAYMENT_STATUS_CANCELED;
            $payment->save();

            Util::sendEmailUser([
                'text' => 'Pedido cancelado com sucesso, Obrigado por fazer parte do LAVAME',
                'subject' => 'Pedido cancelado com sucesso',
            ]);

            DB::commit();

            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
        ];
    }

    public function repeatService(array $options = null)
    {
        //está incompleto
        $this->checkOrderByUser($options);
        $newOrder = Order::query()->where('id', '=', $options['order_id'])->first()->replicate();
        $newOrder->id = Str::uuid();
        $newOrder->save();
    }

    public function checkCoupon(array $options = null)
    {
        $coupon = 0;

        if (Arr::get($options, 'coupon_code')) {

            $getCoupon = Coupon::query()->where('code', '=', Arr::get($options, 'coupon_code'))
                ->from('user.coupon as c')
                ->join('user.list_user_coupon as lup', 'c.id', '=', 'lup.coupon_id')
                ->where('lup.user_id', '=', $options['user_id'] ?? auth()->user()->id)->first();

            if (!$getCoupon) {
                throw new Exception(Lang::get('coupon.not_found_coupon'), 200);
            }

            $coupon = Coupon::query()->where('code', '=', Arr::get($options, 'coupon_code'))->first();

            if ($coupon->amount < 1) {
                throw new Exception(Lang::get('coupon.sold_off'), 200);
            } else if ($coupon->expiration_date < Carbon::now()->format('Y-m-d H:i:s')) {
                throw new Exception(Lang::get('coupon.expiration_date') . ' no dia: ' . $coupon->expiration_date, 200);
            } else if ($coupon->minimum_value > $options['value_total']) {
                throw new Exception(Lang::get('coupon.minimum_value') . ' de: R$:' . $coupon->minimum_value . ',00 reais', 200);
            } else if ($getCoupon->used) {
                throw new Exception(Lang::get('coupon.coupon_used'));
            } else if (Arr::get($options, 'company_id')) {
                $couponUser = CouponUser::query()
                    ->where('coupon_id', '=', $coupon->id)
                    ->where('user_id', '=', Arr::get($options, 'user_id') ?? auth()->user()->id)->first();

                $couponUser->used = true;
                $couponUser->used_date = Carbon::now()->format('Y-m-d H:i:s');
                $couponUser->save();

                $coupon->amount -= 1;
                $coupon->save();
            }

            return $coupon;
        }
    }

    private function checkOrderByUser(array $options = null)
    {
        $checkUser = new UserRepo();
        if (!$checkUser->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            $checkUser->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

            $checkOrderByUser = Order::query()
                ->selectRaw('company.order.*')
                ->join('company.list_company_user as lcu', 'company.order.company_id', '=', 'lcu.company_id')
                ->where('company.order.id', Arr::get($options, 'order_id'))
                ->where(function ($query) use ($options) {
                    $query->orWhere('company.order.user_id', '=', Arr::get($options, 'user_id') ?? auth()->user()->id)
                        ->orWhere('lcu.user_id', '=', Arr::get($options, 'user_id') ?? auth()->user()->id);
                })->exists();

            if (!$checkOrderByUser)
                throw new Exception(Lang::get('common.permission'), 200);
        }
    }

    private function filterAllCompany(&$data, $options)
    {

        if (Arr::get($options, 'order_id')) {
            $data->join('company.order as co', 'c.id', '=', 'co.company_id')
                ->where('co.id', '=', Arr::get($options, 'order_id'));
        }

        if (Arr::get($options, 'company_id')) {
            $data->where('c.id', '=', $options['company_id']);
        }

        if (Arr::get($options, 'service_name') || Arr::get($options, 'min_value') || Arr::get($options, 'max_value')) {
            $data->join('company.list_company_service as cs', 'c.id', '=', 'cs.company_id')
                ->join('company.list_service_modality as sm', 'cs.service_id', '=', 'sm.service_id');
        }

        if (Arr::get($options, 'service_name')) {
            $data->join('company.service as s', 'sm.service_id', '=', 's.id');
            $data->where('s.name', 'ilike', Arr::get($options, 'service_name'));
        }

        if (Arr::get($options, 'min_value') || Arr::get($options, 'min_value') === 0) {
            $data->where('sm.value', '>=', (int)$options['min_value']);
        }

        if (Arr::get($options, 'max_value')) {
            $data->where('sm.value', '<=', (int)$options['max_value']);
        }

        if (Arr::get($options, 'open') == "yes") {
            $hour = Carbon::now()->format('H:i:s');
            $data->where('c.opening_time', '<=', $hour)
                ->where('c.closing_time', '>=', $hour);
        }

        if (Arr::get($options, 'details_company')) {
            $data->join('company.list_company_user as cu', 'c.id', '=', 'cu.company_id')
                ->join('user.user as us', 'cu.user_id', '=', 'us.id')
                ->join('user.person as up', 'us.person_id', '=', 'up.id');
        }
    }
}
