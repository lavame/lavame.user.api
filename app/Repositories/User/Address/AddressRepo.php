<?php

namespace App\Repositories\User\Address;

use App\Helpers\Constants;
use App\Models\Address\Address;
use App\Models\Address\City;
use App\Models\Address\ListAddress;
use App\Models\Address\State;
use App\Repositories\User\UserRepo;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AddressRepo
{
    public function getAddress(array $options = null)
    {
        $this->checkAddressByUser($options);

        if (!Arr::get($options, 'address_id') && !app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $data = Address::query()
            ->with(Arr::get($options, 'relations') ?? ['city', 'state']);

        if (Arr::get($options, 'address_id'))
            $data->where('id', Arr::get($options, 'address_id'));

        return Arr::get($options, 'address_id') ? $data->first() : $data->get();
    }

    public function getCompanyAddress(array $options = null)
    {
        $this->checkAddressByUser($options);

        if (!Arr::get($options, 'address_id') && !app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $data = Address::query()
            ->with(Arr::get($options, 'relations') ?? ['city', 'state']);

        if (Arr::get($options, 'address_id'))
            $data->where('id', Arr::get($options, 'address_id'));

        return $data->get();
    }

    public function getAddressByUser(array $options = null)
    {
        $this->checkAddressByUser($options);

        $user = app()->make(UserRepo::class)
            ->getUserInformation(['user_id' => Arr::get($options, 'user_id') ?? auth()->user()->id]);

        if (!$user)
            throw new Exception(Lang::get('common.error_show'), 200);

        $data = ListAddress::query()
            ->with(Arr::get($options, 'relations') ?? [])
            ->where('active', '=', true);

        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || Arr::get($options, 'user_id'))
            $data->where('list_person_address.person_id', '=', $user->person_id ?? auth()->user()->id);


        return $data->get();
    }

    public function store(array $options = null)
    {
        DB::beginTransaction();

        if (Arr::get($options, 'address_id')) {
            $address = Address::query()
                ->where('id', Arr::get($options, 'address_id'))
                ->first();
        } else {
            $address = new Address();
            $address->id = Str::uuid();
        }

        $address->zip_code   = Arr::get($options, 'address_zip_code')   ?? $address->zip_code;
        $address->number     = Arr::get($options, 'address_number')     ?? $address->number;
        $address->street     = Arr::get($options, 'address_street')     ?? $address->street;
        $address->complement = Arr::get($options, 'address_complement') ?? $address->complement;
        $address->city_id    = Arr::get($options, 'address_city_id')    ?? $address->city_id;
        $address->state_id   = Arr::get($options, 'address_state_id')   ?? $address->state_id;
        $address->save();

        $this->storeByUser([
            'user_id'      => Arr::get($options, 'user_id'),
            'address_id'   => $address->id,
            'address_main' => Arr::get($options, 'address_main'),
        ]);

        DB::commit();
    }

    public function deleteByUser(array $options = null)
    {
        $this->checkAddressByUser($options);

        $user = app()->make(UserRepo::class)
            ->getUserInformation(['user_id' => Arr::get($options, 'user_id') ?? auth()->user()->id, 'relations' => ['person']]);

        $listAddress = ListAddress::query()
            ->where('person_id', $user->person_id)
            ->where('address_id', Arr::get($options, 'address_id'))->first();

        $listAddress->active = false;
        $listAddress->save();
    }

    public function allFeatures()
    {
        $cites = City::query()->get()->ToArray();
        $states = State::query()->get()->ToArray();

        return [
            "cites" => $cites,
            "states" => $states,
        ];
    }

    private function storeByUser(array $options = null)
    {
        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $user = app()->make(UserRepo::class)
            ->getUserInformation(['user_id' => Arr::get($options, 'user_id'), 'relations' => ['person']]);

        $checkAddressByUser = ListAddress::query()
            ->where('person_id', $user->person_id)
            ->where('address_id', Arr::get($options, 'address_id'))->first();

        if (Arr::get($options, 'address_main')) {
            $checkMain = ListAddress::query()
                ->where('person_id', $user->person_id)
                ->where('main', '=', true)->first();

            if ($checkMain) {
                $checkMain->main = false;
                $checkMain->save();
            }
        }

        if (!$checkAddressByUser) {
            $addressByUser = new ListAddress();
            $addressByUser->id = Str::uuid();
            $addressByUser->person_id  = $user->person_id;
            $addressByUser->address_id = Arr::get($options, 'address_id');
            $addressByUser->main       = Arr::get($options, 'address_main') ?? false;
            $addressByUser->active     = true;
            $addressByUser->save();
        } else {
            $checkAddressByUser->main = Arr::get($options, 'address_main') ?? false;
            $checkAddressByUser->save();
        }
    }

    private function checkAddressByUser(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            $user = app()->make(UserRepo::class)
                ->getUserInformation(['user_id' => Arr::get($options, 'user_id') ?? auth()->user()->id]);

            $checkVehicleByUser = ListAddress::query()
                ->where('person_id', $user->person_id);

            if (Arr::get($options, 'address_id'))
                $checkVehicleByUser->where('address_id', Arr::get($options, 'address_id'));

            if (!$checkVehicleByUser->exists())
                throw new Exception(Lang::get('common.permission'));
        }
    }
}
