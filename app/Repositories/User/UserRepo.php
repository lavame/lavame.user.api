<?php

namespace App\Repositories\User;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Person\Person;
use App\Models\User\User;
use App\Repositories\User\Group\GroupRepo;
use App\Repositories\User\Person\PersonRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserRepo
{
    public function getUserInformation(array $options = null)
    {
        $this->checkPermission(Arr::get($options, 'user_login'), Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $data = User::query()
            ->select('user.id', 'user.login', 'user.status', 'user.person_id', 'user.created_at', 'user.status', 'user.status_date_at')
            ->with(Arr::get($options, 'relations') ?? ['groups']);

        if (!Arr::get($options, 'user_all')) {
            $data->where('id', Arr::get($options, 'user_id') ?? auth()->user()->id);
        } else if (Arr::get($options, 'user_login')) {
            $data->where('login', Arr::get($options, 'user_login'));
        }

        return Arr::get($options, 'user_all') ? $data->get() : $data->first();
    }

    public function store(array $options)
    {
        $exists = User::query()
            ->leftJoin('user.person', 'user.user.person_id', '=', 'user.person.id')
            ->where(function ($query) use ($options) {
                $query->orWhere('user.user.login', '=', Arr::get($options, 'user_login'))
                    ->orWhere('user.person.email', '=', Arr::get($options, 'user_email'));
            })->exists();

        if ($exists) {
            throw new Exception(Lang::get('user.create_user.login_email_exists'), 409);
        }

        $user       = new User();
        $user->id   = Str::uuid();

        DB::beginTransaction();

        $personRepo = new PersonRepo();
        $person = $personRepo->store($options);

        $user->login          = Arr::get($options, 'user_login');
        $user->password       = bcrypt(Arr::get($options, 'user_password'));
        $user->status         = true;
        $user->status_date_at = null;
        $user->person_id      = $person->id;
        $user->save();

        Util::sendEmailUser([
            'user_name' => Arr::get($options, 'user_name'),
            'user_email' => Arr::get($options, 'user_email'),
            'text' => 'Cadastro feito com sucesso, Bem vindo ao LAVAME',
            'subject' => 'Cadastro concluído',
        ]);

        DB::commit();
    }

    public function update(array $options)
    {
        $user = '';
        $this->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $users = $this->checkLoginOrEmail($options)->get();

        if (count($users) > 1) {
            throw new Exception(Lang::get('user.create_user.login_email_exists'), 409);
        }

        $user = User::query()->where('id', '=', Arr::get($options, 'user_id') ?? auth()->user()->id)->first();

        DB::beginTransaction();

        $user->login          = Arr::get($options, 'user_login') ? Arr::get($options, 'user_login') : $user->login;
        $user->password       = Arr::get($options, 'user_password') ? bcrypt(Arr::get($options, 'user_password')) : $user->password;
        $user->status         = Arr::get($options, 'user_status') ?? true;
        $user->status_date_at = Arr::get($options, 'user_status') ? Carbon::now()->format('Y-m-d H:i:s') : null;
        $user->save();

        $person = Person::query()->where('id', '=', $user->person_id)->first();
        $person->name       = Arr::get($options, 'user_name') ?? $person->name;
        $person->birth_date = Arr::get($options, 'user_birth_date') ?
            Util::formatDatePatternUSA(Arr::get($options, 'user_birth_date')) : $person->birth_date;
        $person->cnh        = Arr::get($options, 'user_cnh')   ?? $person->cnh;
        $person->cpf        = Arr::get($options, 'user_cpf')   ?? $person->cpf; //validar cpf como aniversario
        $person->email      = Arr::get($options, 'user_email') ?? $person->email;
        $person->phone      = Arr::get($options, 'user_phone') ?? $person->phone; //tirar a mascara do fone
        $person->sex_id     = Arr::get($options, 'user_sex')   ?? $person->sex_id;
        $person->save();


        Util::sendEmailUser([
            'user_name' => Arr::get($options, 'user_name'),
            'user_email' => Arr::get($options, 'user_email'),
            'text' => 'Edição feita com sucesso, caso não tenha sido você, orientamos a mudar sua senha',
            'subject' => 'Editar cadastro concluído',
        ]);

        DB::commit();
    }

    public function delete(array $options)
    {
        $user = $this->getUserInformation(['login' => Arr::get($options, 'user_login'), 'relations' => ['person']]);
        $user->status = false;
        $user->save();
    }

    public function remember(array $options)
    {
        $person = Person::query()
            ->where('email', '=', Arr::get($options, 'user_email'))->first();

        if ($person) {

            DB::beginTransaction();

            $newPassword =  rand(1000, 99999999);

            $user = User::query()->where('person_id', '=', $person->id)->first();
            $user->password = bcrypt($newPassword);
            $user->save();

            Util::sendEmailUser([
                'user_name' => $person->name,
                'user_email' => $person->email,
                'text' => 'Recuperação de senha foi feita com sucesso, sua nova senha é: ' . $newPassword,
                'subject' => 'Recuperação de de senha',
            ]);

            DB::commit();
        }
    }

    public function checkPermissionGroup($group)
    {
        $groups = new GroupRepo();
        $groups = $groups->getGroupByUser([
            'user_id' => auth()->user()->id,
            'select'  => ['group_id']
        ]);

        $groupsArray = [];
        foreach ($groups as $value) {
            $groupsArray[] = $value->group_id;
        }

        if (is_array($group)) {
            foreach ($group as $value) {
                if (in_array($value, $groupsArray)) {
                    return true; // Tem permissão
                }
            }
        } else {
            if (in_array($group, $groupsArray)) {
                return true; // Tem permissão
            }
        }

        return false; // Não tem permissão
    }

    public function checkPermission($login = null, $id = null, array $groups)
    {
        if (auth()->user()) {
            $checkGroup = $this->checkPermissionGroup($groups);

            if (!auth()->user()->status && !$checkGroup)
                throw new Exception(Lang::get('user.actions_after_delete'), 409);

            $informationUser = '';
            $informationAuth = '';

            if ($login) {
                $informationUser = $login;
                $informationAuth = auth()->user()->login;
            } else if ($id) {
                $informationUser = $id;
                $informationAuth = auth()->user()->id;
            }

            if (($informationUser != $informationAuth) && !$checkGroup)
                throw new Exception(Lang::get('common.permission'), 409);
        }
    }

    public function checkLoginOrEmail($options)
    {
        return User::query()
            ->leftJoin('user.person', 'user.user.person_id', '=', 'user.person.id')
            ->where(function ($query) use ($options) {
                if (Arr::get($options, 'user_login')) {
                    $query->orWhere('user.user.login', '=', Arr::get($options, 'user_login'));
                }
                if (Arr::get($options, 'user_email')) {
                    $query->orWhere('user.person.email', '=', Arr::get($options, 'user_email'));
                }
            });
    }
}
