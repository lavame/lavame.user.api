<?php

namespace App\Repositories\User\Vehicle;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Vehicle\Vehicle;
use App\Models\Vehicle\VehicleBrand;
use App\Models\Vehicle\VehicleColor;
use App\Models\Vehicle\VehicleModality;
use App\Models\Vehicle\VehicleModel;
use App\Models\Vehicle\VehicleYear;
use App\Models\Vehicle\VehicleUser;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class VehicleRepo
{
    public function getVehicle(array $options = null)
    {
        $this->checkVehicleByUser($options);

        $data = Vehicle::query()
            ->with(Arr::get($options, 'relations') ?? []);

        if (Arr::get($options, 'vehicle_id'))
            $data->where('id', Arr::get($options, 'vehicle_id'));

        return Arr::get($options, 'user_all') ? $data->get() : $data->first();
    }

    public function getVehicleByUser(array $options = null)
    {
        $data = VehicleUser::query()
            ->with(Arr::get($options, 'relations') ?? [])
            ->where('active', '=', true);

        if (
            !app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) ||
            (app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) && !Arr::get($options, 'all')) ||
            Arr::get($options, 'user_id')
        ) {
            $data->where('list_vehicle.user_id', Arr::get($options, 'user_id') ?? auth()->user()->id);
        }

        if (Arr::get($options, 'vehicle_id'))
            $data->where('list_vehicle.vehicle_id', Arr::get($options, 'vehicle_id'));

        return $data->get();
    }

    public function store(array $options = null)
    {
        $vehicle = '';

        if (Arr::get($options, 'vehicle_id')) {
            $this->checkVehicleByUser($options);

            $vehicle = Vehicle::query()
                ->where('id', Arr::get($options, 'vehicle_id'))
                ->first();

            if (!$vehicle) {
                throw new Exception(Lang::get('common.permission'), 200);
            }

            $vehicle->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        } else {
            $vehicle         = new Vehicle();
            $vehicle->id     = Str::uuid();

            $vehicle->created_at = Carbon::now()->format('Y-m-d H:i:s');
        }

        DB::beginTransaction();

        $vehicle->license_plate = Arr::get($options, 'vehicle_license_plate');
        $vehicle->name        = Arr::get($options, 'vehicle_name');
        $vehicle->description = Arr::get($options, 'vehicle_description');
        $vehicle->model_id    = Arr::get($options, 'vehicle_model_id');
        $vehicle->year_id     = Arr::get($options, 'vehicle_year_id');;
        $vehicle->brand_id    = Arr::get($options, 'vehicle_brand_id');
        $vehicle->color_id    = Arr::get($options, 'vehicle_color_id');
        $vehicle->modality_id = Arr::get($options, 'vehicle_modality_id');
        $vehicle->save();

        if (!Arr::get($options, 'vehicle_id')) {
            $this->storeByUser([
                'user_id'    => Arr::get($options, 'user_id'),
                'vehicle_id' => $vehicle->id
            ]);
        }


        Util::sendEmailUser([
            'text' => 'Veículo com o apelido "' .  $vehicle->name . '" foi cadastrado/editado com sucesso',
            'subject' => 'Veículo cadastrado/editado com sucesso',
        ]);

        DB::commit();
    }

    public function deleteByUser(array $options = null)
    {
        $this->checkVehicleByUser($options);

        $vehicleUser = VehicleUser::query()
            ->where('vehicle_id', Arr::get($options, 'vehicle_id'))
            ->where('user_id', Arr::get($options, 'user_id') ?? auth()->user()->id)->first();

        $vehicleUser->active = false;
        $vehicleUser->save();

        Util::sendEmailUser([
            'text' => 'Veículo com o apelido "' . Arr::get($options, 'vehicle_name') . '" foi deletado com sucesso',
            'subject' => 'Veículo deletado com sucesso',
        ]);
    }

    public function putMain(array $options = null)
    {
        $this->checkVehicleByUser($options);
        $vehicle = VehicleUser::query()
            ->where('user_id', Arr::get($options, 'user_id') ?? auth()->user()->id)->get();

        foreach ($vehicle as $value) {
            if ($value->vehicle_id === $options['vehicle_id']) {
                $value->main = true;
            } else {
                $value->main = false;
            }
            $value->save();
        }
    }

    public function storeFeatures(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $name = 'App\Models\Vehicle\\' . Arr::get($options, 'class');

        if (Arr::get($options, 'features_id')) {
            $features = $name::query()
                ->where('id', Arr::get($options, 'features_id'))->first();

            $features->name = Arr::get($options, 'name');
            $features->save();
        } else {
            $features       = new $name();
            $features->name = Arr::get($options, 'name');
            $features->id   = Str::uuid();
            $features->save();
        }
    }

    public function allFeatures()
    {
        $brands = VehicleBrand::query()->get()->ToArray();
        $colors = VehicleColor::query()->get()->ToArray();
        $models = VehicleModel::query()->get()->ToArray();
        $years = VehicleYear::query()->get()->ToArray();
        $modality = VehicleModality::query()->get()->ToArray();


        usort($models, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });

        usort($brands, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        usort($models, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        usort($modality, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        usort($colors, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });

        return [
            "brands" => $brands,
            "colors" => $colors,
            "models" => $models,
            "years" => $years,
            "modality" => $modality,

        ];
    }

    private function storeByUser(array $options = null)
    {
        $checkVehicleByUser = VehicleUser::query()
            ->where('vehicle_id', Arr::get($options, 'vehicle_id'))
            ->where('user_id', Arr::get($options, 'user_id'))->exists();

        if (!$checkVehicleByUser) {
            $addressByUser = new VehicleUser();
            $addressByUser->id = Str::uuid();
            $addressByUser->user_id    = Arr::get($options, 'user_id') ?? auth()->user()->id;
            $addressByUser->vehicle_id = Arr::get($options, 'vehicle_id');
            $addressByUser->save();
        }
    }

    private function checkVehicleByUser(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN])) {
            app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

            $checkVehicleByUser = VehicleUser::query()
                ->where('vehicle_id', Arr::get($options, 'vehicle_id'))
                ->where('user_id', Arr::get($options, 'user_id') ?? auth()->user()->id)->exists();

            if (!$checkVehicleByUser)
                throw new Exception(Lang::get('common.permission'), 200);
        }
    }
}
