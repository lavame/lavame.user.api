<?php

namespace App\Repositories\User\Coupon;

use App\Helpers\Constants;
use App\Helpers\Util;
use App\Models\Coupon\Coupon;
use App\Models\Coupon\CouponUser;
use App\Repositories\User\UserRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CouponRepo
{
    public function getCoupon(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $data = Coupon::query()
            ->with(Arr::get($options, 'relations') ?? []);

        if (Arr::get($options, 'coupon_id'))
            $data->where('id', Arr::get($options, 'coupon_id'));

        return $data->get();
    }

    public function getCouponByUser(array $options = null)
    {
        app()->make(UserRepo::class)->checkPermission(null, Arr::get($options, 'user_id'), [Constants::GROUP_ADMIN]);

        $data = CouponUser::query()
            ->selectRaw("TO_CHAR(c.expiration_date, 'DD/MM/YYYY HH24:MI:SS') as expiration_date,
                TO_CHAR(list_user_coupon.used_date, 'DD/MM/YYYY HH24:MI:SS') as used_date, list_user_coupon.used,
                c.name, c.id, c.description, c.amount, c.minimum_value, c.discount_value, c.status, c.code")
            ->join('user.coupon as c', 'list_user_coupon.coupon_id', '=', 'c.id');

        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]) || Arr::get($options, 'user_id'))
            $data->where('list_user_coupon.user_id', Arr::get($options, 'user_id') ?? auth()->user()->id);

        if (Arr::get($options, 'coupon_id'))
            $data->where('list_user_coupon.coupon_id', Arr::get($options, 'coupon_id'));

        return $data->get();
    }

    public function store(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $coupon = '';
        $currentHourly = Carbon::now();
        $date          = Util::formatDateTimePatternUSA(Arr::get($options, 'coupon_expiration_date'));

        if ($currentHourly->diffInMinutes($date, false) < 0)
            throw new Exception(Lang::get('coupon.create_coupon.error_data'), 200);

        if (Arr::get($options, 'coupon_amount') == 0)
            throw new Exception(Lang::get('coupon.create_coupon.error_amount'), 200);

        if (Arr::get($options, 'coupon_id')) {
            $coupon = Coupon::query()
                ->where('id', Arr::get($options, 'coupon_id'))
                ->first();
        } else {
            $coupon = new Coupon();
            $coupon->id = Str::uuid();
            $coupon->create_coupon_user_id = auth()->user()->id;
            $coupon->created_at            = Carbon::now()->format('Y-m-d H:i:s');
        }

        $coupon->name            = Arr::get($options, 'coupon_name')          ?? $coupon->name;
        $coupon->description     = Arr::get($options, 'coupon_description')   ?? $coupon->description;
        $coupon->amount          = Arr::get($options, 'coupon_amount')        ?? $coupon->amount;
        $coupon->minimum_value   = Arr::get($options, 'coupon_minimum_value') ?? $coupon->minimum_value;
        $coupon->expiration_date = $date;
        $coupon->status          = Arr::get($options, 'coupon_status') ?? $coupon->status;
        $coupon->save();
    }

    public function storeByUser(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        DB::beginTransaction();

        $checkCouponByUser = CouponUser::query()
            ->where('user_id', Arr::get($options, 'user_id'))
            ->where('coupon_id', Arr::get($options, 'coupon_id'))->exists();

        $coupon = Coupon::query()->where('id', '=', Arr::get($options, 'coupon_id'))->first();

        if ($coupon->amount <= 0) {
            throw new Exception(Lang::get('coupon.sold_off'), 200);
        } else if (!$checkCouponByUser) {
            $couponByUser = new CouponUser();
            $couponByUser->id         = Str::uuid();
            $couponByUser->user_id    = Arr::get($options, 'user_id');
            $couponByUser->coupon_id  = Arr::get($options, 'coupon_id');
            $couponByUser->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $couponByUser->save();

            $coupon->amount -= 1;
            $coupon->save();
        }

        DB::commit();
    }

    public function deleteByUser(array $options = null)
    {
        if (!app()->make(UserRepo::class)->checkPermissionGroup([Constants::GROUP_ADMIN]))
            throw new Exception(Lang::get('common.permission'), 200);

        $couponUser = CouponUser::query()
            ->where('user_id', Arr::get($options, 'user_id'))
            ->where('coupon_id', Arr::get($options, 'coupon_id'))->first();

        if ($couponUser) {
            $couponUser->disabled    = true;
            $couponUser->disabled_by = auth()->user()->id;
            $couponUser->save();
        } else {
            throw new Exception(Lang::get('common.error_show'));
        }
    }
}
