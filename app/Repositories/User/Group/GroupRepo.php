<?php

namespace App\Repositories\User\Group;

use App\Models\Group\Group;
use App\Models\Group\GroupUser;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class GroupRepo
{
    public function getGroup(array $options = null)
    {
        $data = Group::query()
            ->with(Arr::get($options, 'relations') ?? []);

        if (Arr::get($options, 'group_id'))
            $data->where('id', Arr::get($options, 'group_id'));

        return $data->get();
    }

    public function getGroupByUser(array $options = null)
    {
        $groupUser = GroupUser::query()
            ->select(Arr::get($options, 'select', ['user_id', 'group_id']))
            ->with('user');

        if (Arr::get($options, 'user_id')) {
            $groupUser->where('user_id', Arr::get($options, 'user_id'));
        }

        if (Arr::get($options, 'group_id')) {
            $groupUser->where('group_id', Arr::get($options, 'group_id'));
        }

        return $groupUser->get();
    }

    public function store(array $options = null)
    {
        $group = '';

        if (Arr::get($options, 'group_id')) {
            $group = Group::query()
                ->where('id', Arr::get($options, 'group_id'))->first();

            if (!$group)
                throw new Exception(Lang::get('common.error_show'), 200);
        } else {
            $group     = new Group();
            $group->id = Str::uuid();
        }

        $group->name        = Arr::get($options, 'group_name')        ?? $group->name;
        $group->description = Arr::get($options, 'group_description') ?? $group->description;
        $group->save();
    }

    public function storeByUser(array $options = null)
    {

        $groupUser = GroupUser::query()
            ->where('group_id', '=', Arr::get($options, 'group_id'))
            ->where('user_id', '=', Arr::get($options, 'user_id'))->exists();

        if (!$groupUser) {
            $groupUser = new GroupUser();
            $groupUser->group_id = Arr::get($options, 'group_id');
            $groupUser->user_id  = Arr::get($options, 'user_id');
            $groupUser->save();
        }
    }

    public function deleteByUser(array $options = null)
    {

        GroupUser::query()
            ->where('user_id', '=', Arr::get($options, 'user_id'))
            ->where('group_id', '=', Arr::get($options, 'group_id'))->delete();
    }
}
