<?php

use App\Http\Controllers\Company\Address\CompanyAddressController;
use App\Http\Controllers\Company\Bank\CompanyBankController;
use App\Http\Controllers\Company\CompanyController;
use App\Http\Controllers\Company\Order\CompanyOrderController;
use App\Http\Controllers\Company\Service\CompanyServiceController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\User\Coupon\CouponController;
use App\Http\Controllers\Login\LoginController;
use App\Http\Controllers\User\Group\GroupController;
use App\Http\Controllers\User\Address\AddressController;
use App\Http\Controllers\User\Order\UserOrderController;
use App\Http\Controllers\User\Person\PersonController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\Vehicle\VehicleController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/'], function () {
    Route::post('/login', [LoginController::class, 'login'])
        ->name('user.login');
    Route::post('/store', [UserController::class, 'store'])
        ->name('user.store');
    Route::get('/checkLoginOrEmail', [UserController::class, 'checkLoginOrEmail'])
        ->name('user.checkLoginOrEmail');
    Route::post('/remember', [UserController::class, 'remember'])
        ->name('user.remember');
});

Route::group(['middleware' => 'auth-jwt'], function () {
    Route::group(['prefix' => '/'], function () {
        Route::post('/logout', [LoginController::class, 'logout'])
            ->name('user.logout');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', [UserController::class, 'show'])
            ->name('user.show');
        Route::patch('/edit', [UserController::class, 'update'])
            ->name('user.edit');
        Route::delete('/delete', [UserController::class, 'delete'])
            ->name('user.delete');
        Route::get('/dashboard', [DashboardController::class, 'userDashboard'])
            ->name('user.dashboard');

        Route::group(['prefix' => 'address'], function () {
            Route::get('/', [AddressController::class, 'show'])
                ->name('address.show');
            Route::get('/showByUser', [AddressController::class, 'showByUser'])
                ->name('address.showByUser');
            Route::post('/store', [AddressController::class, 'store'])
                ->name('address.store');
            Route::patch('/update', [AddressController::class, 'update'])
                ->name('address.update');
            Route::delete('/deleteByUser', [AddressController::class, 'deleteByUser'])
                ->name('address.deleteByUser');
            Route::get('/allFeatures', [AddressController::class, 'allFeatures'])
                ->name('address.AllFeatures');
        });

        Route::group(['prefix' => 'coupon'], function () {
            Route::get('/', [CouponController::class, 'show'])
                ->name('coupon.show');
            Route::get('/showByUser', [CouponController::class, 'showByUser'])
                ->name('coupon.showByUser');
            Route::post('/store', [CouponController::class, 'store'])
                ->name('coupon.store');
            Route::patch('/update', [CouponController::class, 'update'])
                ->name('coupon.update');
            Route::post('/storeByUser', [CouponController::class, 'storeByUser'])
                ->name('coupon.storeByUser');
            Route::delete('/deleteByUser', [CouponController::class, 'deleteByUser'])
                ->name('coupon.deleteByUser');
        });

        Route::group(['prefix' => 'group'], function () {
            Route::get('/', [GroupController::class, 'show'])
                ->name('group.show');
            Route::get('/showByUser', [GroupController::class, 'showByUser'])
                ->name('group.showByUser');
            Route::post('/store', [GroupController::class, 'store'])
                ->name('group.store');
            Route::patch('/update', [GroupController::class, 'update'])
                ->name('group.update');
            Route::post('/storeByUser', [GroupController::class, 'storeByUser'])
                ->name('group.storeByUser');
            Route::delete('/delete', [GroupController::class, 'deleteByUser'])
                ->name('group.delete');
        });

        Route::group(['prefix' => 'person'], function () {
            Route::get('/', [PersonController::class, 'show'])
                ->name('person.show');
            Route::post('/store', [PersonController::class, 'store'])
                ->name('person.store');
            Route::patch('/update', [PersonController::class, 'update'])
                ->name('person.update');
        });

        Route::group(['prefix' => 'vehicle'], function () {
            Route::get('/', [VehicleController::class, 'show'])
                ->name('vehicle.show');
            Route::get('/showByUser', [VehicleController::class, 'showByUser'])
                ->name('vehicle.showByUser');
            Route::post('/store', [VehicleController::class, 'store'])
                ->name('vehicle.store');
            Route::patch('/update', [VehicleController::class, 'update'])
                ->name('vehicle.update');
            Route::patch('/putMain', [VehicleController::class, 'putMain'])
                ->name('vehicle.putMain');
            Route::delete('/deleteByUser', [VehicleController::class, 'deleteByUser'])
                ->name('vehicle.deleteByUser');
            Route::post('/storeFeatures', [VehicleController::class, 'storeFeatures'])
                ->name('vehicle.storeFeatures');
            Route::patch('/updateFeatures', [VehicleController::class, 'updateFeatures'])
                ->name('vehicle.updateFeatures');
            Route::get('/allFeatures', [VehicleController::class, 'allFeatures'])
                ->name('vehicle.AllFeatures');
        });

        Route::group(['prefix' => 'order'], function () {
            Route::get('/', [UserOrderController::class, 'show'])
                ->name('order.show');
            Route::get('/showByUser', [UserOrderController::class, 'showByUser'])
                ->name('order.showByUser');
            Route::post('/store', [UserOrderController::class, 'store'])
                ->name('order.store');
            Route::post('/cancel', [UserOrderController::class, 'cancel'])
                ->name('order.cancel');
            Route::get('/allCompany', [UserOrderController::class, 'allCompany'])
                ->name('order.allCompany');
            Route::get('/allService', [UserOrderController::class, 'allService'])
                ->name('order.allService');
            Route::get('/allPayments', [UserOrderController::class, 'allPayments'])
                ->name('order.allPayments');
            Route::get('/allOrderStatus', [UserOrderController::class, 'allOrderStatus'])
                ->name('order.allOrderStatus');
            Route::get('/serviceByVehicle', [UserOrderController::class, 'serviceByVehicle'])
                ->name('order.serviceByVehicle');
            Route::get('/checkCoupon', [UserOrderController::class, 'checkCoupon'])
                ->name('order.checkCoupon');
        });
    });

    Route::group(['prefix' => 'company'], function () {
        Route::get('/', [CompanyController::class, 'show'])
            ->name('company.show');
        Route::get('/showByUser', [CompanyController::class, 'showByUser'])
            ->name('company.showByUser');
        Route::post('/store', [CompanyController::class, 'store'])
            ->name('company.store');
        Route::patch('/edit', [CompanyController::class, 'update'])
            ->name('company.edit');
        Route::delete('/deleteByUser', [CompanyController::class, 'deleteByUser'])
            ->name('company.delete');
        Route::get('/checkUserByCompany', [CompanyController::class, 'checkUserByCompany'])
            ->name('company.checkUserByCompany');
        Route::get('/dashboard', [DashboardController::class, 'companyDashboard'])
            ->name('company.dashboard');
        Route::post('/activeCompany', [CompanyController::class, 'activeCompany'])
            ->name('company.activeCompany');

        Route::group(['prefix' => 'address'], function () {
            Route::get('/', [CompanyAddressController::class, 'show'])
                ->name('address.showCompany');
            Route::post('/store', [CompanyAddressController::class, 'store'])
                ->name('address.store');
            Route::delete('/deleteByCompany', [CompanyAddressController::class, 'deleteByCompany'])
                ->name('address.deleteByCompany');
        });

        Route::group(['prefix' => 'bank'], function () {
            Route::get('/', [CompanyBankController::class, 'show'])
                ->name('bank.showCompany');
            Route::post('/store', [CompanyBankController::class, 'store'])
                ->name('bank.store');
            Route::post('/withdrawMoney', [CompanyBankController::class, 'withdrawMoney'])
                ->name('bank.withdrawMoney');
            Route::patch('/update', [CompanyBankController::class, 'update'])
                ->name('bank.update');
            Route::delete('/deleteByCompany', [CompanyBankController::class, 'deleteByCompany'])
                ->name('bank.deleteByCompany');
        });

        Route::group(['prefix' => 'service'], function () {
            Route::get('/', [CompanyServiceController::class, 'show'])
                ->name('service.showCompany');
            Route::get('/showByCompany', [CompanyServiceController::class, 'showByCompany'])
                ->name('service.showByCompany');
            Route::post('/store', [CompanyServiceController::class, 'store'])
                ->name('service.store');
            Route::delete('/deleteByCompany', [CompanyServiceController::class, 'deleteByCompany'])
                ->name('service.deleteByCompany');
            Route::get('/allFeatures', [CompanyServiceController::class, 'allFeatures'])
                ->name('service.AllFeatures');
        });

        Route::group(['prefix' => 'order'], function () {
            Route::get('/showByCompany', [CompanyOrderController::class, 'showByCompany'])
                ->name('order.showByCompany');
            Route::post('/update', [CompanyOrderController::class, 'update'])
                ->name('order.update');
            Route::post('/addValueDirt', [CompanyOrderController::class, 'addValueDirt'])
                ->name('order.addValueDirt');
        });
    });
});
