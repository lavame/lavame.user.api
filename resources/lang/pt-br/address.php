<?php

return [
    'create_address' => [
        'success'      => 'Sucesso, seu endereço foi cadastrado com sucesso.',
        'edit_success' => 'Sucesso, seu endereço foi editado com sucesso.',
        'error'        => 'Erro, por favor entrar em contato com o Administrador',
        'error_data'   => 'Erro, data menor que a atual',
        'error_amount' => 'Erro, o valor do cupom está zerado',
    ],
    'request' => [
        'address_zip_code'   =>  'Código postal',
        'address_street'     =>  'Endereço',
        'address_city_id'    =>  'Cidade',
        'address_state_id'   =>  'Estado',
    ],
];
