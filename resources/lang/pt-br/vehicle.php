
<?php

return [
    'create_vehicle' => [
        'success'        => 'Sucesso, seu Veículo foi cadastrado com sucesso.',
        'edit_success'   => 'Sucesso, seu Veículo foi editado com sucesso.',
        'delete_success' => 'Sucesso, seu Veículo foi deletado com sucesso',
        'put_main' => 'Sucesso, seu Veículo foi colocado como principal com sucesso',
    ],
    'request' => [
        'vehicle_model_id'    =>  'Modelo',
        'vehicle_year_id'     =>  'Ano',
        'vehicle_brand_id'    =>  'Marca',
        'vehicle_color_id'    =>  'Cor',
        'vehicle_modality_id' =>  'Categoria',
    ],
];
