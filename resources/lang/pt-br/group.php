<?php

return [
    'create_group' => [
        'success'      => 'Sucesso, seu grupo foi feito com sucesso.',
        'edit_success' => 'Sucesso, seu grupo foi editado com sucesso.',
        'name_exists'  => 'Aviso, nome do grupo digitado já existe.',
    ],
];
