<?php

return [
    'not_found' => 'Empresa não encontrada',
    'delete' => 'Empresa deletada com sucesso',
    'active_company' => 'Empresa Liberada com sucesso',
    'request' => [
        'company_name' => 'Nome',
        'company_cnpj' => 'Cnpj',
        'company_email' => 'E-mail',
        'company_closing_time' => 'Fechamento',
        'company_opening_time' => 'Abertura',
        'company_phone' => 'Telefone',
        'bank_name' => 'Nome do banco',
        'bank_agency' => 'Agencia do banco',
        'bank_account' => 'Conta do banco',

    ],
    'create_company' => [
        'email_exists' => 'Aviso, e-mail digitado já existe.',
        'login_email_exists' => 'Aviso, cnpj ou e-mail digitados já existem.',
    ],
];
