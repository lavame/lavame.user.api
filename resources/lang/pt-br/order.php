<?php

return [
    'not_found_coupon' => 'Cupom não encontrado',
    'sold_off'         => 'Cupom esgotado',
    'found_coupon'     => 'Cupom encontrado',
    'dirt'             => 'Taxa adicionada com sucesso',
    'create' => [
        'success'      => 'Sucesso, seu pedido foi cadastrado com sucesso.',
        'update'       => 'Sucesso, seu pedido foi editado com sucesso.',
        'error_cancel' => 'Erro, seu pedido não foi cancelado.',
        'cancel'       => 'Sucesso, seu pedido foi cancelado com sucesso.',
    ],
    'request' => [
        'coupon_name'            => 'Nome',
        'coupon_description'     => 'Descrição',
        'coupon_amount'          => 'Valor',
        'coupon_minimum_value'   => 'Valor minimo',
        'coupon_expiration_date' => 'Data expiração',
        'coupon_id'              => 'Código cupom',
        'user_id'                => 'Usuário',
    ],
];
