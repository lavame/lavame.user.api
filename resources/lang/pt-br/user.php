<?php

return [
    'incorrect_login'      => 'Usuário e/ou senha inválido(s)',
    'enter_a_user'         => 'Digite um login/id para a pesquisa',
    'delete'               => 'Usuário deletado com sucesso',
    'actions_after_delete' => 'Você não tem permissão para essa ação, usuário foi deletado anteriormente, entre em contato com o Administrador',
    'user_not_exists'      => 'Usuário não encontrado',
    'user_remember'        => 'Uma nova senha foi enviada para o e-mail informado',
    'dashboard' => 'Dados do dashboard',
    'request' => [
        'user_login'      => 'Login',
        'user_password'   => 'Senha',
        'user_status'     => 'Status',
        'user_name'       => 'Nome',
        'user_cpf'        => 'Cpf',
        'user_email'      => 'E-mail',

    ],
    'create_user' => [
        'login_exists' => 'Aviso, login digitado já existe.',
        'email_exists' => 'Aviso, e-mail digitado já existe.',
        'login_email_exists' => 'Aviso, login ou e-mail digitados já existem.',
    ],
    'show_user' => [
        'required' => 'É obrigatório o tipo do valor ser Uuid para o campo id.',
    ]
];
