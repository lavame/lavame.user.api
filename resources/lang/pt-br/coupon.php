<?php

return [
    'not_found_coupon' => 'Cupom não encontrado',
    'sold_off'         => 'Cupom esgotado',
    'minimum_value'    => 'Valor mínimo não atingindo',
    'expiration_date'  => 'Cupom expirado',
    'coupon_used'      => 'Cupom já usado',

    'create_coupon' => [
        'success'      => 'Sucesso, seu cupom foi cadastrado foi feito com sucesso.',
        'edit_success' => 'Sucesso, seu cupom foi editado com sucesso.',
        'error'        => 'Erro, por favor entrar em contato com o Administrador',
        'error_data'   => 'Erro, data menor que a atual',
        'error_amount' => 'Erro, o valor do cupom está zerado',

    ],
    'request' => [
        'coupon_name'            => 'Nome',
        'coupon_description'     => 'Descrição',
        'coupon_amount'          => 'Valor',
        'coupon_minimum_value'   => 'Valor minimo',
        'coupon_expiration_date' => 'Data expiração',
        'coupon_id'              => 'Código cupom',
        'user_id'                => 'Usuário',
    ],
];
