<?php

return [
    'error_show'       => 'Erro, não encontramos nada, entre em contato com o Administrador.',
    'required'         => 'É obrigatório um valor para o campo :attribute.',
    'success'          => 'Sucesso',
    'permission'       => 'Você não tem permissão para essa ação',
    'login_permission' => 'Faça o login para continuar',
    'token_expired'    => 'Seu token expirou, logar novamente no sistema',
    'create_user' => [
        'success'      => 'Sucesso, seu cadastro foi feito com sucesso.',
        'edit_success' => 'Sucesso, seu cadastro foi editado com sucesso.',
    ],
];
