<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head></head>

<body>
    <style></style>
    <div class="card" style="border:1px solid #DDD;box-shadow:0 2px 6px #999;margin:0 auto;width:768px">
        <div class="header"
            style="background: linear-gradient(270deg, rgba(94, 212, 247, 0.691648) 0%, rgba(127, 212, 236, 0.599722) 53.65%, rgba(247, 211, 197, 0.271017) 75%, rgba(236, 149, 116, 0.648505) 100%);
            transform: rotate(-180deg);">
            <br>
        </div>
        <div style="margin: 3rem">
            <div style="color: #007AC3; font-size: 22px; font-weight: 400;margin-left: 40%; margin-bottom: 2rem">
                LAVAME
            </div>

            <div style="margin-bottom: 1.5rem">
                <div
                    style="
                    font-style: normal;
                    font-weight: 325;
                    font-size: 18px;
                    color: #404040;">
                    {{ $text }}
                </div>
                <div style="margin-top: 1rem">
                    Mensagem enviada em: {{ $date }}
                </div>
            </div>

            <div>
                <div
                    style="
                    font-style: normal;
                    font-weight: 400;
                    font-size: 16px;
                    margin-bottom: 0.5rem">
                    Dados do usuário:
                </div>

                <div
                    style="
                    font-style: normal;
                    font-weight: 400;
                    font-size: 14px;
                    margin-bottom: 1.5rem">
                    <div>
                        nome: {{ $user }}
                    </div>
                    <div>
                        E-mail: {{ $email }}
                    </div>
                </div>
            </div>

            <div
                style="
                font-style: normal;
                font-weight: 400;
                font-size: 18px;
                color: #404040;
                margin-bottom: 1.5rem">
                E-mail gerado automaticamente pela plataforma.
            </div>
        </div>
        <div class="header" style="height: 88px;
            background: rgba(0, 104, 167, 0.1);">
            <br>
        </div>
    </div>
</body>

</html>
